all: build_gui build_cli build_server 
run: server cli


DOXYGEN=doxygen

clean:
	$(MAKE) clean -C ./src/client/maze_gui_client/

ifeq ($(OS),Windows_NT)
build_gui:
	$(MAKE) --file=Makefile.release -C ./src/client/maze_gui_client/
else
build_gui:
	$(MAKE) -C ./src/client/maze_gui_client/
endif

build_cli:
	$(MAKE) --file=Makefile.cli -C ./src/client/maze_gui_client/

build_server:
	$(MAKE) -C ./src/server/maze_server/

doxygen:
	$(DOXYGEN) Doxyfile

ifeq ($(OS),Windows_NT)
server:
	cd src/server/maze_server/ && start bludiste2014-server.exe &

cli:
	cd src/client/maze_gui_client/ && bludiste2014-cli.exe

gui:
	cd src/client/maze_gui_client/ && bludiste2014.exe
else
server:
	cd src/server/maze_server/ && ./bludiste2014-server &

cli:
	cd src/client/maze_gui_client/ && ./bludiste2014-cli  

gui:
	cd src/client/maze_gui_client/ && ./bludiste2014 
endif

#./src/client/maze_gui_client/bludiste2014-cli

