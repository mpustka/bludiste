  _____________________   _____  ________      _____  ___________
  \______   \_   _____/  /  _  \ \______ \    /     \ \_   _____/
   |       _/|    __)_  /  /_\  \ |    |  \  /  \ /  \ |    __)_ 
   |    |   \|        \/    |    \|    `   \/    Y    \|        \
   |____|_  /_______  /\____|__  /_______  /\____|__  /_______  /
          \/        \/         \/        \/         \/        \/

  AUTORI:   MICHAL PUSTKA (xpustk00@stud.fit.vutbr.cz)
            MARTIN RONCKA (xronck00@stud.fit.vutbr.cz)
          
  PROJEKT:  IJC_BLUDISTE (2014)
  
  SPUSTENI HRY:
    Nejdrive je nutno zapnout server se specifikovanym portem
    (vychozi port pro client i server je (30000))
    
    synopsis-unix: ./bludiste2014-server 30000 
    synopsis-win: bludiste2014-server.exe 30000 
    ----------------------------------------------------

    Pote dle prostredi muzete pouzit klientskou aplikaci.
    Pro praci v konzoli je pripravena aplikace bludiste2014-cli
    (vychozi server je nastaven na 127.0.0.1 a port 30000)
    
    synopsis-unix: ./bludiste2014-cli eva.fit.vutbr.cz 30000
    synopsis-win: bludiste2014-cli.exe eva.fit.vutbr.cz 30000
    
    Ovladani: 
        pohyb:      UP - OTOC SE NAHORU
                    LEFT - OTOC SE DOLEVA
                    DOWN - OTOC SE DOLU
                    RIGHT - OTOC SE DOPRAVA

                    GO - BEZ
                    STOP - ZASTAV SE

        interakce:  KILL - ZABIJ HRACE
                    TAKE - VEZMI KLIC PRED/POD HRACEM
                    OPEN - OTEVRI BRANU PRED HRACEM

        Udalosti:   Hrac je zabit v pripade dotyku strazce
                    Pri projeti cilem (42) hra konci

    Ukonceni je provedeno zadanim prikazu QUIT.
    ----------------------------------------------------
    
    Jako okenni aplikaci lze pouzit bludiste2014 vyuzivajici QT.
    Na unixu je nutna podpora X11.

    Konfigurace pripojeni na server je realizovana pomoci okennich
    dialogu. Je mozne vytvorit hru nebo se na libovolnou bezici hru
    pripojit. V pripade ze je hra obsazena, klient je automaticky
    odpojen.
    
    Ovladani: 
        pohyb:      W - NAHORU A BEZ
                    A - DOLEVA A BEZ
                    S - DOLU A BEZ
                    D - DOPRAVA A BEZ

                    G - BEZ
                    MEZERNIK - ZASTAVI HRACE

        interakce:  K - ZABIJ HRACE (hrac je obnoven na pocatecni pozici)
                    T - VEZMI KLIC PRED/POD HRACEM
                    O - OTEVRI BRANU PRED HRACEM

        Udalosti:   Hrac je zabit v pripade dotyku strazce
                    Pri projeti cilem (dort) hra konci

    Ukonceni klientske aplikace je realizovano v menu Aplikace->Ukoncit.
    ----------------------------------------------------
    
    Server se ukonci zapsanim prikazu QUIT do konzole serveru Pote
    jsou vsichni klienti odpojeni z her
    
    ENjOY.
