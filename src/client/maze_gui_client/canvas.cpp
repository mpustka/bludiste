//! Soubor canvas.cpp
/*! Platno pro vykraslovani.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "canvas.h"


Canvas::Canvas(int sizeTile, QGraphicsView *view, QTWrapper *wrapper)
    : QGraphicsScene(0, 0, view->width(), view->height())
{
    view->setBackgroundBrush(QBrush(Qt::black, Qt::SolidPattern));

    view->setScene(this);
    _sizeTile = sizeTile;
    _wrapper = wrapper;
}

void Canvas::drawForeground(QPainter *painter, const QRectF &rect)
{
    _wrapper->Draw(painter, rect);

    painter->setPen(Qt::gray);

 /*   for (qreal x = rect.left(); x <= rect.right(); x += _sizeTile)
        painter->drawLine(QLineF(x, rect.top(), x, rect.bottom()));
    for (qreal y = rect.top(); y <= rect.bottom(); y += _sizeTile)
         painter->drawLine(QLineF(rect.left(), y, rect.right(), y));*/
}
