//! Soubor canvas.h
/*! Platno pro vykraslovani.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef CANVAS_H
#define CANVAS_H

#include "qtwrapper.h"

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainter>
#include <QRectF>

class QTWrapper;

class Canvas : public QGraphicsScene
{
    Q_OBJECT
    public:
        explicit Canvas(int _sizeTile, QGraphicsView *view, QTWrapper *wrapper);

    private:
        int _sizeTile;
        QTWrapper *_wrapper;

    protected:
        void drawForeground(QPainter * painter, const QRectF & rect);

    signals:

    public slots:

};

#endif // CANVAS_H
