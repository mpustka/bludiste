//! Soubor connection_dialog.cpp
/*! Dialog pro zadani portu a serveru.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "connection_dialog.h"
#include "ui_connectiondialog.h"

ConnectionDialog::ConnectionDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConnectionDialog)
{
    ui->setupUi(this);
}

std::string ConnectionDialog::GetServer()
{
    return ui->lineEdit->text().toUtf8().constData();
}

std::string ConnectionDialog::GetPort()
{
    return QString::number(ui->spinBox->value()).toUtf8().constData();
}

ConnectionDialog::~ConnectionDialog()
{
    delete ui;
}
