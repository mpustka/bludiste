//! Soubor connection_thread.cpp
/*! Protokol na strane klienta, komunikuje ze serverem a posila mu prikazy.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "connection_thread.h"

#include <unistd.h>
#include <string>
#include <sstream>

#define BUF_LEN 1024
#define TERM    "\r\n"

using namespace std;

Connection_thread::Connection_thread(ThreadQueue *messages, unsigned int id)
    : NetThread(messages, id)
{

}

Connection_thread::~Connection_thread()
{
    if(_client != NULL)
    {
        string str = "QUIT";
        _client->Send(str + TERM);

        delete _client;
    }
}

//TODO: koreknti ukonceni hry ve vsech stavech.
void Connection_thread::Execute()
{
    std::chrono::milliseconds duration(1);
    int finiteStateMachine = START;
    string port, server, map, speed, xMap, yMap, game;

    while(!_stop.load())
    {
        ThreadMessage *message = NULL;

        switch(finiteStateMachine)
        {
            case START:

                Send(THREADID_CLIENT, new ThreadMessage(MSQ_GET_CONNECTION_DATA));
                finiteStateMachine = WATING_CONNECTION_DATA;

            break;

            case WATING_CONNECTION_DATA:

                if((message = Get()) != NULL)
                {
                    if(message->GetType() == MSQ_SEND_CONNECTION_DATA)
                    {
                        server = message->GetData(0);
                        port = message->GetData(1);

                        delete message;

                        if(_client != NULL)
                            delete _client;

                        cout << "c: Server connect " + server + ":" + port << endl;
                        _client = new Client(server, port);

                        if(_client->Connect() != Client::CLIENT_OK)
                        {
                            std::cerr << "Connection was not established." << std::endl;
                            Send(THREADID_CLIENT, new ThreadMessage(MSQ_CONNECTION_ERROR));
                            finiteStateMachine = WAITING_FALSE;
                            break;
                        }else{
                            finiteStateMachine = WATING_GET_MAPS;
                        }
                    }
                 }

            break;

            case WATING_GET_MAPS:
            {
                string str = _client->GetLine();

                if(str.empty() == false)
                {
                    cout << "s: " + str << endl;
                    finiteStateMachine = WATING_SELECT_MAP;

                    if(str.compare(0, 3, "100") == 0)
                    {
                        message = new ThreadMessage(MSQ_GET_MAP_CREATE_GAME);
                        message->SetData(str.c_str());
                        Send(THREADID_CLIENT, message);
                    }else{
                        Send(THREADID_CLIENT, new ThreadMessage(MSQ_CONNECTION_ERROR));
                    }
                }
                break;
            }

            case WATING_SELECT_MAP:
            {
                if((message = Get()) != NULL)
                {
                    game = message->GetData(0);
                    string str = "";
                    if(message->GetType() == MSQ_SEND_MAP_CREATE_GAME)
                    {
                        map = message->GetData(1);
                        speed = message->GetData(2);

                        str = "CREATE " + game + " " + speed + " " + map + TERM;
                    }

                    str = str + "JOIN " + game + TERM;


                    _client->Send(str);
                    cout << "c: " + str << endl;
                    delete message;

                    finiteStateMachine = WATING_GET_MAP_BOUNDS;
                }

                break;
            }

            case WATING_GET_MAP_BOUNDS:
            {
                string code;
                string str = _client->GetLine();

                if(str.empty() == false)
                {
                    cout << "s: " + str << endl;
                    //   str = "120 dd 23 23";
                    std::stringstream stream(str);
                    stream >> code;
                    if (code == "120")
                    {
                        stream >> map >> xMap >> yMap;

                        message = new ThreadMessage(MSQ_SEND_MAP_BOUNDS);

                        message->SetData(xMap.c_str());
                        message->SetData(yMap.c_str());

                        finiteStateMachine = WATING_DATA;
                        Send(THREADID_CLIENT, message);
                    }
                    else
                    {
                        Send(THREADID_CLIENT, new ThreadMessage(MSQ_CONNECTION_ERROR));
                        finiteStateMachine = WAITING_FALSE;
                    }
                }

                break;
            }

            case WATING_DATA:
            {
                if ((message = Get())!= NULL)       //!< Zkontrollujeme zpravy ze vstupniho rozhrani.
                {
                    //! Pokud se jedna o typ prikazu. Zasleme jej na server s prefixem CMD.
                    if (message->GetType() == MSQ_SEND_COMMAND)
                        _client->Send(string("CMD ") + message->GetData(0) + TERM);

                    delete message;
                }
                string x, y, data, code;

                string str = _client->GetLine();
                if (str == "")
                    break;
                std::stringstream stream(str);

                stream >> code;
                if(code == "130")
                {
                    //cout << "s: " + str << endl;
                    stream >> data >> x >> y;

                    message = new ThreadMessage(MSQ_SET_DISPLAY);

                    message->SetData(data.c_str());
                    message->SetData(x.c_str());
                    message->SetData(y.c_str());
                    Send(THREADID_CLIENT, message);

                }else if(code == "140"){

                    cout << "s: " + str << endl;
                    message = new ThreadMessage(MSQ_GAME_END);

                    string score = "";

                    while(score != "END")
                    {
                        stream >> score;
                        message->SetData(score.c_str());
                    }

                    Send(THREADID_CLIENT, message);
                }else if(code == "150"){                    //!< Kod pro Repoty.

                    message = new ThreadMessage(MSQ_INFO);  //!< Vytvorime novou zpravu.
                    message->SetData(str.c_str());        //!< Ulozime do ni prijatou zparvu.
                    Send(THREADID_CLIENT, message);         //!< A zasleme ji zobrazovacimu rozhrani.

                }else if(code.empty()){

                }else{
                    cout << "s: " + str << endl;
                    Send(THREADID_CLIENT, new ThreadMessage(MSQ_CONNECTION_ERROR));
                }
                break;
            }

            default:
                break;
        }

        std::this_thread::sleep_for(duration);
    }
}
