//! Soubor connection_thread.h
/*! Protokol na strane klienta, komunikuje ze serverem a posila mu prikazy.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef CONNECTION_THREAD_H
#define CONNECTION_THREAD_H

#include "../../common/net_thread.h"
#include "../../common/client.h"

//zde bude komunikacni protokol mezi vlakny

#define MSQ_GET_CONNECTION_DATA 1
#define MSQ_SEND_CONNECTION_DATA 2

#define MSQ_GET_MAP 3
#define MSQ_GET_MAP_CREATE_GAME 4

#define MSQ_SEND_MAP 5
#define MSQ_SEND_MAP_CREATE_GAME 6
#define MSQ_SEND_JOIN_GAME 7
#define MSQ_SEND_MAP_BOUNDS 8
#define MSQ_SEND_COMMAND 9

#define MSQ_INFO 10
#define MSQ_SET_DISPLAY 11

#define MSQ_CONNECTION_ERROR 12
#define MSQ_GAME_END 13

#define THREADID_CLIENT 1
#define THREADID_CONNECTION 2

//state
#define START 0
#define WATING_CONNECTION_DATA 1
#define WATING_GET_MAPS 2
#define WATING_GET_MAP_BOUNDS 3
#define WATING_SELECT_MAP 4

#define WATING_DATA 5
#define WAITING_FALSE -1

class Connection_thread : public NetThread
{
    private:
        Client *_client = NULL;
    public:

        Connection_thread(ThreadQueue *messages, unsigned int id);
        virtual ~Connection_thread();

        void Execute();
};

#endif // CONNECTION_THREAD_H
