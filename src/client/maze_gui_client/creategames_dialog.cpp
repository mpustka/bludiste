//! Soubor creategames_dialog.cpp
/*! Dialog pro vytvoreni hry.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */


#include "creategames_dialog.h"
#include "ui_dialogcreategames.h"

DialogCreateGames::DialogCreateGames(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogCreateGames)
{
    ui->setupUi(this);
}

void DialogCreateGames::SetMaps(std::vector<std::string> *_maps)
{
    for(auto it = _maps->begin(); it != _maps->end(); ++it)
        ui->comboBox->addItem(QString::fromStdString(*it));
}

std::string DialogCreateGames::GetMap()
{
    return  ui->comboBox->currentText().toUtf8().constData();
}

std::string DialogCreateGames::GetName()
{
    return ui->lineEdit->text().toUtf8().constData();
}

std::string DialogCreateGames::GetSpeed()
{
    return QString::number(ui->doubleSpinBox->value()).toUtf8().constData();
}

DialogCreateGames::~DialogCreateGames()
{
    delete ui;
}
