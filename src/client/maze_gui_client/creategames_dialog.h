//! Soubor creategames_dialog.h
/*! Dialog pro vytvoreni hry.
    Generovano.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef DIALOGCREATEGAMES_H
#define DIALOGCREATEGAMES_H

#include <QDialog>
#include <QStringListModel>
#include <string>

namespace Ui {
class DialogCreateGames;
}

class DialogCreateGames : public QDialog
{
    Q_OBJECT

public:
    explicit DialogCreateGames(QWidget *parent = 0);
    ~DialogCreateGames();

    void SetMaps(std::vector<std::string> *_maps);
    std::string GetMap();
    std::string GetSpeed();
    std::string GetName();

private:
    Ui::DialogCreateGames *ui;
};

#endif // DIALOGCREATEGAMES_H
