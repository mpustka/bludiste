//! Soubor createorjoin_dialog.cpp
/*! Dialog, kde uzivatel rozhodne jestli se pripoji ke stavajici hre nebo zalozi novou.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "createorjoin_dialog.h"
#include "ui_createorjoindialog.h"

CreateOrJoinDialog::CreateOrJoinDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CreateOrJoinDialog)
{
    ui->setupUi(this);
}

CreateOrJoinDialog::~CreateOrJoinDialog()
{
    delete ui;
}

bool CreateOrJoinDialog::GetStatus()
{
    return _create;
}

void CreateOrJoinDialog::on_join_button_clicked()
{
    _create = false;
    close();
}

void CreateOrJoinDialog::on_create_button_clicked()
{
    _create = true;
    close();
}
