//! Soubor createorjoin_dialog.h
/*! Dialog, kde uzivatel rozhodne jestli se pripoji ke stavajici hre nebo zalozi novou.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef CREATEORJOIN_DIALOG_H
#define CREATEORJOIN_DIALOG_H

#include <QDialog>

namespace Ui {
class CreateOrJoinDialog;
}

class CreateOrJoinDialog : public QDialog
{
    Q_OBJECT

    public:
        explicit CreateOrJoinDialog(QWidget *parent = 0);
        ~CreateOrJoinDialog();

        bool GetStatus();

    private slots:
        void on_join_button_clicked();

        void on_create_button_clicked();

private:

        Ui::CreateOrJoinDialog *ui;
        bool _create = false;
};

#endif // CREATEORJOIN_DIALOG_H
