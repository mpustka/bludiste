#ifndef DIALOGMAPS_H
#define DIALOGMAPS_H

#include <QDialog>

namespace Ui {
class DialogMaps;
}

class DialogMaps : public QDialog
{
    Q_OBJECT

public:
    explicit DialogMaps(QWidget *parent = 0);
    ~DialogMaps();

private:
    Ui::DialogMaps *ui;
};

#endif // DIALOGMAPS_H
