//! Soubor games_dialog.cpp
/*! Dialog pro pripojeni ke hre.
    Generovano.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include <string>

#include "games_dialog.h"
#include "ui_dialoggames.h"

DialogGames::DialogGames(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogGames)
{
    ui->setupUi(this);
}

void DialogGames::SetGames(std::vector<std::string> *_games)
{
    for(auto it = _games->begin(); it != _games->end(); ++it)
        ui->comboBox->addItem(QString::fromStdString(*it));
}

std::string DialogGames::GetGame()
{
    return ui->comboBox->currentText().toUtf8().constData();
}

DialogGames::~DialogGames()
{
    delete ui;
}
