//! Soubor games_dialog.h
/*! Dialog pro pripojeni ke hre.
    Generovano.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef DIALOGGAMES_H
#define DIALOGGAMES_H

#include <QDialog>

namespace Ui {
class DialogGames;
}

class DialogGames : public QDialog
{
    Q_OBJECT

public:
    explicit DialogGames(QWidget *parent = 0);
    ~DialogGames();

    void SetGames(std::vector<std::string> *_games);
    std::string GetGame();

private:
    Ui::DialogGames *ui;
};

#endif // DIALOGGAMES_H
