//! Soubor main.cpp
/*! Spolecny main jak pro QT tak pro CLI klienta.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */


// Vkladani hlavickovych souboru pro podmineny preklad CLI/GUI clienta
#ifndef CLI_CLIENT

    #include "mainform.h"
    #include "canvas.h"
    #include <QApplication>
    #include <QGraphicsView>
    #include <QMessageBox>

#else

    #include "main_window.h"

#endif


//! Vstupni bod programu, je vytvoreno okno CLI/GUI ktere dale ridi chod programu
int main(int argc, char *argv[])
{
#ifndef CLI_CLIENT
    QApplication a(argc, argv);
    MainForm window;

    window.show();

    return a.exec();

    /*try
    {*//*
        MainForm window;
        window.show();*/
        /*catch(const std::exception &e){

        QMessageBox::critical
        (
            NULL,
            "Bludiste",
            e.what()
        );

        return 1;
    }*/
#else

    MainWindow window(argc, argv);
    window.GiveControl();

#endif
    return EXIT_SUCCESS;
}
