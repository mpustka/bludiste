//! main_window.cpp
/*! Soubor obsahuje implementace metod tridy MainWindow

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "main_window.h"

const std::string MainWindow::CONTROL = "GATE CLOSED/OPENED []/__, FINISH 42, KEY = ?., GUARD = :E\n"
                                        "AC: GO | STOP | RIGHT | LEFT | UP | DOWN | TAKE | OPEN | QUIT";

//! Konstruktor tridy MainWindow zajistuje inicializace potrebnych atributu
MainWindow::MainWindow(int argc, char *argv[])
{
    _x = 0;
    _y = 0;
    _command = "";

    if (argc == 3)
    {
        _server = argv[1];
        _port = argv[2];
    }

    _display = new Display();
    _mutex = new std::mutex();
    _messages = NetThread::CreateMessageContainer();
    _connection = new Connection_thread(_messages, THREADID_CONNECTION);
}

//! Destruktor zajistuje uvolneni vsecch alokovanych zdroju.
MainWindow::~MainWindow()
{
    delete _mutex;
    delete _display;
}

//! Metoda ridi zpracovani vstupu uzivatele.
/*!

 */
void MainWindow::HandleInput()
{
    char c;
    ThreadMessage *message;

    while (_state.load() != STATE_STOP)
    {
        while (1)
        {
            c = 0;
            c = getch();

            if (c == '\r' || c == '\n')
                break;

            if (c == '\b')
            {
                _command = _command.substr(0, _command.length() - 1);
                printw(" \b");
            }
            else
                _command += c;
        }

        _mutex->lock();

        move(_y + 4, 0);
        clrtoeol();
        clrtoeol();
        move(_y + 4, 0);

        _mutex->unlock();

        if (_command == "QUIT")
        {
            break;
        }
        else if (_command != "")
        {
            message = new ThreadMessage(MSQ_SEND_COMMAND);
            message->SetData(_command.c_str());
            _messages->Send(THREADID_CONNECTION, message);
        }
        _command = "";
    }
    _state.store(STATE_STOP);
}

//! Metoda pro inicializaci TUI.
void InitializeTUI()
{
    initscr();          //!< Zapneme rezim TUI
    start_color();      //!< Nutne inicilaizovat pro pouzivani barev
    curs_set(0);        //!< Zneviditelnime kurzor
}

//! Metoda zajisti vykresleni objektu na dane pozici
void MainWindow::Redraw(char type, int x, int y)
{
    _mutex->lock();
    move(y + 1,x * 2 + 1);
    {
        if (type == 0)
            printw("  ");
        else if (type == 1)
        {
            COLOR_PRINT("#|", COLOR_WALL, COLOR_WALL_ID);
        }
        else if (type == 2)
        {
            COLOR_PRINT("[]", COLOR_GATE, COLOR_GATE_ID);
        }
        else if (type == 3)
        {
            COLOR_PRINT("__", COLOR_GATE, COLOR_GATE_ID);
        }
        else if (type == 4)
        {
            COLOR_PRINT("?.", COLOR_KEY, COLOR_KEY_ID);
        }
        else if (type > 0xF)
        {
            int player = (type & 0xF0) >> 4;
            if (player == 1)
            {
                COLOR_PRINT(":)", COLOR_P1, COLOR_P1_ID);
            }
            else if (player == 2)
            {
                COLOR_PRINT(":]", COLOR_P2, COLOR_P2_ID);
            }
            else if (player == 3)
            {
                COLOR_PRINT("=)", COLOR_P3, COLOR_P3_ID);
            }
            else if (player == 4)
            {
                COLOR_PRINT("xD", COLOR_P4, COLOR_P4_ID);
            }
        }
        else if (type >= 9 && type <= 12)
        {
            COLOR_PRINT(":E", COLOR_GUARD, COLOR_GUARD_ID);
        }
        else if (type >= 12 && type <= 15)
        {
            COLOR_PRINT("42", COLOR_FINISH, COLOR_FINISH_ID);
        }
        else
        {
            printw("@@");
        }
    }

    move(_y + 4, _command.length());

    refresh();
    _mutex->unlock();
}

void MainWindow::Redraw()
{
    _mutex->lock();
    clear();

    init_pair(1, COLOR_WALL);
    attron(A_BOLD | COLOR_PAIR(1));

    for (int j = 0; j <= _x; ++j)
        printw("--");

    printw("\n");
    for (int i = 0; i < _y; ++i)
    {
        printw("|");

        attroff(A_BOLD | COLOR_PAIR(1));
        for (int j = 0; j < _x; ++j)
            printw(" .");

        attron(A_BOLD | COLOR_PAIR(1));
        printw("|\n");
    }

    for (int j = 0; j <= _x; ++j)
        printw("--");


    attroff(A_BOLD | COLOR_PAIR(1));
    move(_y + 2,0);
    printw(MainWindow::CONTROL.c_str());


    refresh();
    _mutex->unlock();
}

void MainWindow::GiveControl()
{
    _connection->Start();
    std::chrono::milliseconds duration(10);

    //! Smycka hlida pripojeni na server
    if (_state.load() != STATE_STOP)
    {
        while ((_message = _messages->Get(THREADID_CLIENT)) == NULL)
            std::this_thread::sleep_for(duration);
        MessageFromConnection(_message);
    }
    //! Uspesne pripjeni/Zalozeni hry
    if (_state.load() != STATE_STOP)
    {
        while ((_message = _messages->Get(THREADID_CLIENT)) == NULL)
            std::this_thread::sleep_for(duration);
        MessageFromConnection(_message);
    }

    //! Nastav rozmery mapy
    if (_state.load() != STATE_STOP)
    {
        while ((_message = _messages->Get(THREADID_CLIENT)) == NULL)
            std::this_thread::sleep_for(duration);
        MessageFromConnection(_message);
    }

    if (_state.load() != STATE_STOP)
    {
        InitializeTUI();
        Redraw();
        refresh();
        std::thread protocolThread(&MainWindow::HandleInput, this);

        while (_state.load() != STATE_STOP)
        {
            while ((_message = _messages->Get(THREADID_CLIENT)) != NULL)
                MessageFromConnection(_message);

            std::this_thread::sleep_for(duration);
        }

        endwin();
        protocolThread.join();
    }

    _connection->Stop();
    delete  _connection;
}

void MainWindow::MessageFromConnection(ThreadMessage *message)
{
    if (message == NULL)
        return;

    ThreadMessage *messageSend = NULL;
    std::string data;

    //! Zpracovani jednotlivych zprav / pozadavku sitove casti klienta
    switch(message->GetType())
    {
        case MSQ_GET_CONNECTION_DATA:

            messageSend = new ThreadMessage(MSQ_SEND_CONNECTION_DATA);

            messageSend->SetData(_server.c_str());
            messageSend->SetData(_port.c_str());

            _messages->Send(THREADID_CONNECTION, messageSend);

        break;

        case MSQ_GET_MAP_CREATE_GAME:

            std::cout << "\nTo join a game type in JOIN\nTo create a game type in CREATE" << std::endl;
            while (1)
            {
                std::cin >> data;
                if (data == "JOIN" )
                {
                    messageSend = new ThreadMessage(MSQ_SEND_JOIN_GAME);
                    std::cout << "Type in the name of the game: ";
                    std::cin >> data;
                    messageSend->SetData(data.c_str());
                    break;
                }
                else if (data == "CREATE")
                {
                    messageSend = new ThreadMessage(MSQ_SEND_MAP_CREATE_GAME);
                    std::cout << "Type in the name of the game: ";
                    std::cin >> data;
                    messageSend->SetData(data.c_str());
                    std::cout << "Select one of the maps: ";
                    std::cin >> data;
                    messageSend->SetData(data.c_str());
                    std::cout << "Specify the speed of the game(0.5 - 5): ";
                    std::cin >> data;
                    messageSend->SetData(data.c_str());
                    break;
                }
            }

            _messages->Send(THREADID_CONNECTION, messageSend);

        break;

        case MSQ_SEND_MAP_BOUNDS:
        {
            _x = atoi(message->GetData(0).c_str());
            _y = atoi(message->GetData(1).c_str());
        }
        break;

        case MSQ_INFO:
        {
            _report = message->GetData(0);
            _mutex->lock();
            move(_y + 5, 0);
            clrtoeol();
            printw(_report.c_str());
            move(_y + 4, 0);
            refresh();
            _mutex->unlock();
        }
        break;

        case MSQ_SET_DISPLAY:
        {
            char data = (char)atoi(message->GetData(0).c_str());
            int x = atoi(message->GetData(1).c_str());
            int y = atoi(message->GetData(2).c_str());

            Redraw(data, x, y);
            _display->Set(x, y, data);
        }

        break;
        case MSQ_CONNECTION_ERROR:
        {
            clear();
            refresh();
            std::cout << "Terminating program";
            _state.store(STATE_STOP);
        }
    }

    delete message;
}
