//! Soubor main_window.h
/*! Hlavni okno Clienta. Zajisti vytvoreni vlakna pro zpracovani pripojeni.
    Vykreslovani jednoducheho TUI na obrazovku a zpracovani vstupu uzivatele.
    Vykreslovani je realizovano knhovnami conie.h (windows) / ncurses.h (unix).

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __MAIN_WINDOW_H__
#define __MAIN_WINDOW_H__

#include "connection_thread.h"
#include "../../common/display.h"

#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <map>
#include <string>
#include <atomic>

#ifdef _WIN32

    #include "curses.h"

#else

    #include <ncurses.h>

#endif // _WIN32

//! Definice barev a ID pro stavebn bloky bludiste pro curses
#define COLOR_WALL_ID 3
#define COLOR_WALL COLOR_YELLOW, COLOR_RED

#define COLOR_KEY_ID 4
#define COLOR_KEY COLOR_YELLOW, COLOR_BLACK

#define COLOR_GATE_ID 5
#define COLOR_GATE COLOR_CYAN, COLOR_BLACK

#define COLOR_GUARD_ID 6
#define COLOR_GUARD COLOR_CYAN, COLOR_BLACK

#define COLOR_P1_ID 7
#define COLOR_P1 COLOR_GREEN, COLOR_BLACK

#define COLOR_P2_ID 8
#define COLOR_P2 COLOR_BLUE, COLOR_BLACK

#define COLOR_P3_ID 9
#define COLOR_P3 COLOR_RED, COLOR_BLACK

#define COLOR_P4_ID 10
#define COLOR_P4 COLOR_YELLOW, COLOR_BLACK

#define COLOR_FINISH_ID 42
#define COLOR_FINISH COLOR_MAGENTA, COLOR_BLACK

//! Makro pro barevny vypis
#define COLOR_PRINT(WORD, COLORS, ID)               \
                                                    \
    {                                               \
        init_pair(ID, COLORS);                      \
        attron(A_BOLD  | COLOR_PAIR(ID));           \
        printw(WORD);                               \
        attroff(A_BOLD  | COLOR_PAIR(ID));          \
    }

//! Define pro ID vlaken
#define CONTROL_THREAD_ID 0
#define CONNECTION_THREAD_ID 1

//! Trida je realizaci Textoveho rozhrani hry bludiste
/*! Zajistuje vytvoreni samostatne sitove casti zajistujicic komunikaci
    se serverem a zpracovani zprav zaslanych timto vlaknem. Zaroven zpracovava
    prkazy klienta a vykreslovani jednoducheho TUI do konzole.
 */
class MainWindow
{
public:
    //! Verejne metody.
    MainWindow(int argc, char *argv[]);
    ~MainWindow();

    void GiveControl();

private:
    //! Privatni atributy.
    int _x, _y;
    std::atomic<int> _state;
    std::string _server = "127.0.0.1";
    std::string _port = "30000";
    std::string _report;
    std::string _command;
    ThreadQueue *_messages;
    ThreadMessage *_message;
    Connection_thread *_connection;
    std::mutex *_mutex;
    Display *_display;

    //! Privatni konstanty.
    static const int STATE_STOP = -1;
    static const int STATE_OK = 0;
    static const int STATE_CONNECTING = 1;
    static const int STATE_RUNNING = 2;
    static const std::string CONTROL;

    //! Privatni metody.
    void Redraw();
    void Redraw(char data, int x, int y);       //! Prekresleni konretniho pole.
    void HandleInput();
    void MessageFromConnection(ThreadMessage* message);
};

#endif // __MAIN_WINDOW_H__
