//! Soubor mainform.cpp
/*! Dialog hlavniho okna.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "mainform.h"
#include "ui_mainform.h"
#include "../../common/display.h"
#include "qtwrapper.h"
#include "sync_thread.h"
#include "connection_thread.h"
#include "connection_dialog.h"
#include "creategames_dialog.h"
#include "games_dialog.h"
#include "createorjoin_dialog.h"

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QMessageBox>
#include <QDebug>

using namespace std;

MainForm::MainForm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainForm)
{
    ui->setupUi(this);

    setFixedSize(size());

    _messages = NetThread::CreateMessageContainer();

    _display = new Display();

    _wrapper = new QTWrapper((QWindow*)this, ui->graphicsView, _display);

    if(!_wrapper->LoadTheme("../textures"))
        throw ios_base::failure("Textures haven't been loaded.");

    ui->graphicsView->hide();
    ui->label->setAlignment(Qt::AlignmentFlag::AlignCenter);
}

void MainForm::MessageFromConnection(ThreadMessage *message)
{
    ThreadMessage *messageSend = NULL;

    switch(message->GetType())
    {
        case MSQ_GET_CONNECTION_DATA:

            messageSend = new ThreadMessage(MSQ_SEND_CONNECTION_DATA);

            messageSend->SetData(_server.c_str());
            messageSend->SetData(_port.c_str());

            _messages->Send(THREADID_CONNECTION, messageSend);

        break;

        case MSQ_GET_MAP_CREATE_GAME:
        {
            QString str = QString::fromStdString(message->GetData(0));

            QStringList list = str.split(' ');

            std::vector<std::string> maps;
            std::vector<std::string> games;

            bool sgames = false;
            bool smaps = false;

            for(int i = 0; i < list.size(); ++i)
            {
                if(list.at(i).isEmpty())
                    continue;

                if(list.at(i) == "Games:")
                {
                    sgames = true;
                    smaps = false;
                    continue;
                }else if(list.at(i) == "Maps:"){
                    sgames = false;
                    smaps = true;
                    continue;
                }

                if(sgames)
                    games.push_back(list.at(i).toUtf8().constData());

                if(smaps)
                    maps.push_back(list.at(i).toUtf8().constData());
            }

            CreateOrJoinDialog *dialog = new  CreateOrJoinDialog(this);
            dialog->setModal(true);
            dialog->exec();

            if(dialog->GetStatus())
            {
                //! vytvor novou hru

                DialogCreateGames *dialogCreate = new  DialogCreateGames(this);
                dialogCreate->setModal(true);

                dialogCreate->SetMaps(&maps);

                if(dialogCreate->exec() == QDialog::Accepted)
                {
                    messageSend = new ThreadMessage(MSQ_SEND_MAP_CREATE_GAME);

                    messageSend->SetData(dialogCreate->GetName().c_str());
                    messageSend->SetData(dialogCreate->GetMap().c_str());
                    messageSend->SetData(dialogCreate->GetSpeed().c_str());

                    _messages->Send(THREADID_CONNECTION, messageSend);
                }else{
                   return;
                }
            }else{
                //! pripoj se ke hre

                DialogGames *dialogJoin = new DialogGames(this);
                dialogJoin->setModal(true);

                dialogJoin->SetGames(&games);

                if(dialogJoin->exec() == QDialog::Accepted)
                {
                    messageSend = new ThreadMessage(MSQ_SEND_JOIN_GAME);

                    messageSend->SetData(dialogJoin->GetGame().c_str());

                    _messages->Send(THREADID_CONNECTION, messageSend);
                }else{
                   return;
                }
            }
        }
        break;

        case MSQ_SEND_MAP_BOUNDS:
        {
            int countWidthTiles = atoi(message->GetData(0).c_str());
            int countHeightTiles = atoi(message->GetData(1).c_str());

            setFixedSize(QSize(countWidthTiles * 32, countHeightTiles * 32 + ui->menuBar->height()));
            _wrapper->StartDraw(countWidthTiles, countHeightTiles);

            ui->label->hide();
            ui->graphicsView->show();
            _gameRunnig = true;
        }
        break;

        case MSQ_SET_DISPLAY:
        {
            char data = (char)atoi(message->GetData(0).c_str());
            int x = atoi(message->GetData(1).c_str());
            int y = atoi(message->GetData(2).c_str());

            _display->Set(x, y, data);

            _wrapper->_canvas->update();
        }
        break;

        case MSQ_GAME_END:
        {
            QMessageBox::warning
            (
                NULL,
                "Bludiste",
                "Game Over!"
            );

            _gameRunnig = false;
            ui->graphicsView->hide();
            ui->label->show();
        }
        break;

        case MSQ_INFO:
        {           
            cout << message->GetData(0) << endl;
        }
        break;

        case MSQ_CONNECTION_ERROR:
        {
            _syncThread->Stop();

            QMessageBox::critical
            (
                NULL,
                "Bludiste",
                "Connection error!"
            );

            ui->label->show();
            ui->graphicsView->hide();
            _gameRunnig = false;
        }
        break;
    }

    delete message;
}

void MainForm::keyPressEvent(QKeyEvent *event)
{
    static int state = 0;

    if(!_gameRunnig || state == event->key())
        return;

    state = event->key();

    ThreadMessage *messageSend = new ThreadMessage(MSQ_SEND_COMMAND);

    if(event->key() == Qt::Key_W)
    {
        messageSend->SetData("UP\r\nCMD GO");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_S){
        messageSend->SetData("DOWN\r\nCMD GO");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_A){
        messageSend->SetData("LEFT\r\nCMD GO");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_D){
        messageSend->SetData("RIGHT\r\nCMD GO");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_T){
        messageSend->SetData("TAKE");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_O){
        messageSend->SetData("OPEN");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_Space){
        messageSend->SetData("STOP");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_G){
        messageSend->SetData("GO");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }else if(event->key() == Qt::Key_K){
        messageSend->SetData("KILL");
        _messages->Send(THREADID_CONNECTION, messageSend);
    }
}

MainForm::~MainForm()
{
    if(_syncThread != NULL)
    {
        _syncThread->Stop();
        _syncThread->wait();

        delete _syncThread;
    }

    delete ui;
    delete _wrapper;
}

void MainForm::on_menu_join_or_create_triggered()
{
    if(_gameRunnig)
        return;

    ConnectionDialog *dialog = new ConnectionDialog(this);
    dialog->setModal(true);

    if(dialog->exec() == QDialog::Accepted)
    {
      _port = dialog->GetPort();
      _server = dialog->GetServer();
      ui->label->setText("Conneting...");

      _syncThread = new SyncThread(_messages, this);
      connect(_syncThread, SIGNAL(MessageToGUI(ThreadMessage*)), this, SLOT(MessageFromConnection(ThreadMessage*)),Qt::QueuedConnection);
      _syncThread->start();
    }
}

void MainForm::on_menu_exit_triggered()
{
    close();
}
