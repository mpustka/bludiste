//! Soubor mainform.h
/*! Dialog hlavniho okna.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef MAINFORM_H
#define MAINFORM_H

#include "qtwrapper.h"
#include "../../common/net_thread.h"
#include "sync_thread.h"

#include <QMainWindow>
#include <QResizeEvent>

#include <mutex>
#include <queue>
#include <map>

namespace Ui
{
class MainForm;
}

class MainForm : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainForm(QWidget *parent = 0);
        ~MainForm();

    protected:
        void keyPressEvent(QKeyEvent *);

    private:

        Ui::MainForm *ui;
        QTWrapper *_wrapper = NULL;

        ThreadQueue *_messages = NULL;

        SyncThread *_syncThread = NULL;
        Display *_display  = NULL;

        std::string _port;
        std::string _server;

        bool _gameRunnig = false;

    public slots:
        void MessageFromConnection(ThreadMessage* message);
    private slots:
        void on_menu_join_or_create_triggered();
        void on_menu_exit_triggered();
};

#endif // MAINFORM_H
