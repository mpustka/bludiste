#-------------------------------------------------
#
# Project created by QtCreator 2014-05-05T20:36:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = maze_gui_client
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11
LIBS += -lws2_32


SOURCES += main.cpp\
        mainform.cpp \
    canvas.cpp \
    qtwrapper.cpp \
    ../../common/client.cpp \
    ../../common/net_thread.cpp \
    connection_thread.cpp \
    sync_thread.cpp \
    connection_dialog.cpp \
    creategames_dialog.cpp \
    games_dialog.cpp \
    createorjoin_dialog.cpp \
    ../../common/display.cpp

HEADERS  += mainform.h \
    canvas.h \
    qtwrapper.h \
    ../../common/client.h \
    ../../common/net_thread.h \
    connection_thread.h \
    sync_thread.h \
    connection_dialog.h \
    creategames_dialog.h \
    games_dialog.h \
    createorjoin_dialog.h \
    ../../common/display.h

FORMS    += mainform.ui \
    connectiondialog.ui \
    dialoggames.ui \
    dialogcreategames.ui \
    createorjoindialog.ui
