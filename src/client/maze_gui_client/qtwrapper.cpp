//! Soubor qtwrapper.cpp
/*! Obaluje celeho qt klienta, stara se o vykreslovani displaje na canvas.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "qtwrapper.h"

#include <QDir>
#include <string>
#include <iostream>

using namespace std;

QTWrapper::QTWrapper(QWindow *window, QGraphicsView *view, Display *display)
{
    _view = view;
    _window = window;
    _display = display;
}

void QTWrapper::StartDraw(int countTilesWidth, int countTilesHeight)
{
   // _window->setWidth(countTilesWidth * _sizeTile);
   // _window->setHeight(countTilesHeight * _sizeTile);
    _display->Reset();
    _canvas = new Canvas(_sizeTile, _view, this);
}

QTWrapper::~QTWrapper()
{
    if(_canvas != NULL)
        delete _canvas;

    foreach(QImage *value, _textures)
        delete value;
}

bool QTWrapper::LoadTheme(const char *themePath)
{
    try
    {
        QDir dir(themePath);
        QString str = dir.absolutePath();
        QStringList files = dir.entryList(QDir::Files | QDir::NoSymLinks);

        for(auto it = files.begin(); it != files.end(); ++it)
           _textures[*it] = new QImage(dir.absolutePath() + "/" + *it);

    }catch(...){
        return false;
    }
    return true;
}

void QTWrapper::Draw(QPainter *painter, const QRectF &rect)
{
    for(auto it = _display->Begin(); it != _display->End(); ++it)
    {
        unsigned char index_texture = it->second->data & 0b00001111;
        unsigned char id = (it->second->data & 0b11110000) >> 4;

        QString key = QString::number(index_texture) + (id == 0 ? "" : QString::number(id));

        if(index_texture != 0)
        {
            Box *box = it->second;
            QString texture = "texture" + key + ".png";
            QImage *image = _textures[texture];

            painter->drawImage(QPoint(rect.left() + box->x * _sizeTile, rect.top() + box->y * _sizeTile), *image);
        }
    }
}
