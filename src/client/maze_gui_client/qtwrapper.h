//! Soubor qtwrapper.h
/*! Obaluje celeho qt klienta, stara se o vykreslovani displaje na canvas.
    Casti generovany.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef QTWRAPPER_H
#define QTWRAPPER_H

#include "canvas.h"
#include "../../common/display.h"

#include <QWindow>
#include <QPainter>
#include <QHash>
#include <QGraphicsView>

class Canvas;

class QTWrapper
{
    private:

        Display *_display;


        QGraphicsView *_view;
        QWindow *_window;

        QHash<QString, QImage*> _textures;

        const int _sizeTile = 32;

    public:
        QTWrapper(QWindow *window, QGraphicsView *view, Display *display);
        ~QTWrapper();

        bool LoadTheme(const char *themePath);
        void StartDraw(int countTilesWidth, int countTilesHeight);
        void Draw(QPainter *painter, const QRectF &rect);
         Canvas *_canvas = NULL;
};

#endif // QTWRAPPER_H
