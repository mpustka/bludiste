//! Soubor sync_thread.cpp
/*! Synchrnonizacni vlakno mezi QT aplikaci a Connection Threadem.
    Generovano.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "sync_thread.h"
#include "connection_thread.h"

using namespace std;

SyncThread::SyncThread(ThreadQueue *messages, QObject* parent)
    : QThread(parent)
{
    _messages = messages;
    _stop.store(false);
}

void SyncThread::run()
{
    chrono::milliseconds duration(1);
    Connection_thread connection(_messages, THREADID_CONNECTION);
    connection.Start();

    while(!_stop.load())
    {
        ThreadMessage *message = NULL;

        if((message = _messages->Get(THREADID_CLIENT)) != NULL)
            emit MessageToGUI(message);

        this_thread::sleep_for(duration);
    }

    connection.Stop();
}

void SyncThread::Stop()
{
    _stop.store(true);
}
