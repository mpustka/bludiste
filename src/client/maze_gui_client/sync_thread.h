//! Soubor sync_thread.h
/*! Synchrnonizacni vlakno mezi QT aplikaci a Connection Threadem.
    Generovano.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef SYNC_THREAD_H
#define SYNC_THREAD_H

#include <QThread>
#include <QObject>
#include "../../common/net_thread.h"

class SyncThread : public QThread
{
    Q_OBJECT

    private:
        ThreadQueue *_messages;
        std::atomic<bool> _stop;

    protected:
        void run();

    public:
        explicit SyncThread(ThreadQueue *messages, QObject* parent = 0);

        void Stop();

    signals:
        void MessageToGUI(ThreadMessage *message);
};

#endif // SYNC_THREAD_H
