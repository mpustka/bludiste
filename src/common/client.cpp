//! Soubor client.cpp
/*! Programova realizace sitoveho klienta tridou Client. Je vytvorenaa abstrakce
    nad sockety. uzivatel je od nich odstiten a ovlada komunikaci pouze pomoci
    inicializace a zasilani zprav pomoci Send a Getline.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "client.h"

//! Konstruktor tridy Client, zajisti naplneni atributu pozadovanymi daty.
Client::Client(std::string hostname, std::string port)
{
    _hostname = hostname;
    _port = port;
    _state = SocketInitialization();
}

//! Destruktor zajistuje pripadne uzavreni socketu a WSACleanup nad winsock.
Client::~Client()
{
    if (_socket)
        closesocket(_socket);

    #ifdef _WIN32
    WSACleanup();
    #endif // _WIN32
}

//! Metoda realizuje pripojeni na serer
/*! Metoda zastinuje pripojeni na server pomoci socketu. Je vytvoreno spoejni s
    se serverem na zadane adrese a portu a nastaveno neblokujici cteni.
    V pripade potreby by toto mo funkcionalita mohla byt dana do metody a volana
    zvlast.
    \return Navratova hodnota, stejne jako stav indikuji uspesnost pripojeni
    \sa Client(), ~Client(), Client()
 */
int Client::Connect()
{
    //! Definice struktury a inicializace ukazetlu.
    struct addrinfo *serverinfo = NULL,
                    *p = NULL,
                    hints;

    //! Nastavime strukturu addrinfo potrebnymi daty.
    ZeroMemory( &hints, sizeof(hints) );    //!< Nejdrive vynulujeme pamet.
    hints.ai_family = AF_UNSPEC;            //!< Nastavime na libovolny IP.
    hints.ai_socktype = SOCK_STREAM;        //!< Pouzijeme socket stream.
    hints.ai_protocol = IPPROTO_TCP;        //!< Specifikujeme TCP protokol.

    //! Ziskame seznam adres pro pripojeni na dany server
    if (getaddrinfo(_hostname.c_str(), _port.c_str(), &hints, &serverinfo) != 0) {
        _state = Client::CLIENT_UTC;
        return Client::CLIENT_UTC;
    }

    //! V cyklu projdeme senzam po prvcich a pokusime se navazat spojeni.
    int res;                                //!< Pro kontorlu hodnot funkci.
    for (p = serverinfo; p != NULL; p = p->ai_next)
    {
        //! Pokusime se vytvorit socket na danem portu.
        _socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (_socket == INVALID_SOCKET)
        {
            std::cerr << strerror(errno) << std::endl;
            _state = Client::CLIENT_UTC;
            return Client::CLIENT_UTC;
        }

        //! Pripojeni na server. V pripade selhane zkusime dalsi polozku.
        res = connect(_socket, p->ai_addr, (int)p->ai_addrlen);
        if (res == SOCKET_ERROR)        //!< Zkontrolujeme zda se pripojeni povedlo.
        {
            closesocket(_socket);       //!< Pokud ne uzvareme ziskany socket.
            _socket = INVALID_SOCKET;   //!< Nastavime jej na nevalidni.
            continue;                   //!< A Zkusime pokracovat dalsi poolozkou.
        }

        _addrinfo = *p;                 //!< Ulozime si informace o pripojeni.
        break;                          //!< V pripade navazani spojeni koncime.
    }

    freeaddrinfo(serverinfo);           //!< Volame funkci pro uvolneni seznamu.

    //! Zkontrolujeme zda bylo spojeni navazano
    if (_socket == INVALID_SOCKET)
    {
        _state = Client::CLIENT_UTC;
        return Client::CLIENT_UTC;
    }

    #ifdef _WIN32
        u_long iMode = 1;
        ioctlsocket(_socket, FIONBIO, &iMode);
    #else
        fcntl(_socket, F_SETFL, O_NONBLOCK);
    #endif

    return Client::CLIENT_OK;
}

//! Inicializace socketu je nutna pro windows.
int Client::SocketInitialization()
{
    #ifdef _WIN32
    WSADATA wsaData; // if this doesn't work
    // MAKEWORD(1,1) for Winsock 1.1, MAKEWORD(2,0) for Winsock 2.0:

    //!
    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0) {
        _state = Client::CLIENT_UTC;
        return Client::CLIENT_UTC;
    }
    #endif // _WIN32

    return Client::CLIENT_OK;
}

//! Getter pro ziskani stavu klienta
int Client::GetStatus()
{
    return this->_state;
}

//! Metoda pro uzavreni spojeni a invalidaci socketu.
int Client::CloseConnection()
{
    if (_socket != INVALID_SOCKET)
    {
        closesocket(_socket);
        _socket = INVALID_SOCKET;
    }

    return 0;
}

//! Rozhrani pro odesilani retezcu serveru.
/*! Metoda pouze prevede retezec do Ceckoveho retezce a odesle jej.
 */
int Client::Send(std::string message)
{
    send(_socket, message.c_str(), message.length(), 0);
    return Client::CLIENT_UTC;
}

//! Metoda zajistuje cteni ze socketu po radcich.
/*! Je nutne cist po znaku, jelikoz uz samotny socket slouzi jako stradac.
 */
std::string Client::GetLine()
{
    std::string line = "";
    char c;
    int ret;

    while (1)
    {
        ret = recv(_socket, &c, 1, 0);
        if (ret <= 0)
            break;

        if (c == '\r')
        {
            recv(_socket, &c, 1, 0);
            break;
        }
        else if (c == '\n')
            break;

        line += c;
    }

    return line;
}
