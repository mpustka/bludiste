//! Soubor client.h
/*! Hlavickovy soubor obsahuje definici tridy Client zastinujici praci se
    sockety. Spojeni se serverem predaym konstrktoru jako hostname a port
    je realizovano pomoci libovolneho protokolu a TCP.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>

//! Vkladani hlavickovych souboru dle platformny.
#ifdef _WIN32

    #define _WIN32_WINNT 0x501
    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>

#else

    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include <fcntl.h>
    #include <sys/types.h>
    #include <arpa/inet.h>

    //! Definice maker pro vytvoreni jednotneho rozhrani na Linux/Windows
    #define closesocket(socket) close(socket)
    #define ZeroMemory(memory, size) std::memset(memory, 0, size)
    #define INVALID_SOCKET ((unsigned int) -1)
    #define SOCKET_ERROR (-1)

#endif

//! Trida Client
/*! Uplne zastinuje praci s klasickymi BSD sockety. Pripojeni je realizovano
    pomoci libovolneho ip protokolu s TCP na transportnim protokolu, tyto
    moznosti by mohli byt nastavitelne pro pripad rozsiritelnosti tridy.
    Komunikace po navazani spojeni je zajistena metodami Send() a Getline().
 */
class Client
{
    private:
        //! Privatni atributy.
        struct addrinfo _addrinfo;
        std::string _hostname;
        std::string _port;
        int _state;
        unsigned int _socket;

        //! Privatni metody.
        int SocketInitialization();

    public:
        //! Verejne konstanty.
        static const int CLIENT_OK = 0;     //!< Stavy klienta: Vse OK.
        static const int CLIENT_UTC = -1;   //!< Nevalidni spojeni.

        //! Verejne metody.
        Client(std::string hostname, std::string port);
        ~Client();

        int GetStatus();                    //!< Metoda pro ziskani stavu klienta.
        int Connect();                      //!< Pro navazani spojeni na server.
        std::string GetLine();              //!< Pro cteni ze socketu po radcich.
        int Send(std::string message);      //!< Pro zasilani retezcu na server.
        int CloseConnection();              //!< Uzavreni spojeni a invalidace sockety.
};

#endif // __CLIENT_H__
