//! Soubor display.cpp
/*! Slouzi pro zobrazovani dat z mapy.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "display.h"

#include <string>
#include <sstream>

using namespace std;

Display::Display()
{
}

int Display::Get(int x, int y)
{
    _mutex.lock();
    stringstream ss;
    ss << x << "'" << y;
    int type;

    Box* box = _buffer[ss.str()];
    if (box == NULL)
        type = 0;
    else
    {
        type = box->data;
    }
    _mutex.unlock();

    return (int) type;
}

void Display::Set(int x, int y, char data)
{
    _mutex.lock();

    stringstream ss;

    ss << x;
    ss << "'";
    ss << y;

    string key = ss.str();

    if(_buffer[key] == NULL)
    {
        Box *box = new Box;
        _buffer[key] = box;
    }

    _buffer[key]->x = x;
    _buffer[key]->y = y;
    _buffer[key]->data = data;

    _mutex.unlock();
}

void Display::SetStatic(int x, int y, char data)
{
    _mutex.lock();

    stringstream ss;

    ss << x;
    ss << "'";
    ss << y;

    string key = ss.str();

    if(_bufferStatic[key] == NULL)
    {
        Box *box = new Box;
        _bufferStatic[key] = box;
    }

    _bufferStatic[key]->x = x;
    _bufferStatic[key]->y = y;
    _bufferStatic[key]->data = data;

    _mutex.unlock();
}

void Display::Reset()
{
    _mutex.lock();

    auto it = _buffer.begin();

    //while(it != _buffer.end())
    while(!_buffer.empty())
    {
       // if(it->second == 0)
      //  {
            _buffer.erase(it++);
       // }else{
      //      ++it;
      //  }
    }

    _mutex.unlock();
}


void Display::Lock()
{
    _mutex.lock();
}

void Display::Unlock()
{
    _mutex.unlock();
}

std::map<string, Box*>::iterator Display::Begin()
{
    return _buffer.begin();
}

std::map<string, Box*>::iterator Display::End()
{
    return _buffer.end();
}

std::map<std::string, Box*>::iterator Display::BeginStatic()
{
    return _bufferStatic.begin();
}

std::map<std::string, Box*>::iterator Display::EndStatic()
{
    return _bufferStatic.end();
}

Display::~Display()
{
    for(auto it = _buffer.begin(); it != _buffer.end(); ++it)
        delete it->second;
}
