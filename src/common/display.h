//! Soubor display.h
/*! Slouzi pro zobrazovani dat z mapy.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <map>
#include <string>
#include <mutex>

#define DIS_WALL 1
#define DIS_GATE_CLOSED 2
#define DIS_GATE_OPENED 3
#define DIS_KEY 4
#define DIS_PLAYER_BOTTOM 5
#define DIS_PLAYER_UP 6
#define DIS_PLAYER_LEFT 7
#define DIS_PLAYER_RIGHT 8

#define DIS_GUARD_BOTTOM 9
#define DIS_GUARD_UP 10
#define DIS_GUARD_LEFT 11
#define DIS_GUARD_RIGHT 12
#define DIS_FINISH 13

struct Box
{
    char data;
    int x, y;
};

class Display
{
    private:

        std::map<std::string, Box*> _buffer;
        std::map<std::string, Box*> _bufferStatic;
        std::mutex _mutex;

    public:

        Display();
        ~Display();

        int Get(int x, int y);
        void Set(int x, int y, char data);
        void SetStatic(int x, int y, char data);

        void Lock();
        void Unlock();

        void Reset();

        std::map<std::string, Box*>::iterator Begin();
        std::map<std::string, Box*>::iterator End();

        std::map<std::string, Box*>::iterator BeginStatic();
        std::map<std::string, Box*>::iterator EndStatic();
};

#endif // DISPLAY_H
