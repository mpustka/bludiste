//! Soubor net_thread.cpp
/*! Trida pro praci s vlakny.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "net_thread.h"

using namespace std;

NetThread::NetThread(ThreadQueue *messages, unsigned int id)
{
    _messages = messages;
    _id = id;

    _thread = NULL;
    _stop.store(false);
}

void NetThread::Execute()
{

}

void NetThread::Start()
{
    if(_thread == NULL)
        _thread = new thread(&NetThread::Execute, this);
}

void NetThread::Stop()
{
    _stop.store(true);

    if(_thread != NULL)
        _thread->join();
}

unsigned int NetThread::GetID()
{
    return _id;
}

ThreadMessage* NetThread::Get()
{
    return _messages->Get(_id);
}

ThreadMessage* NetThread::Get(unsigned int threadId)
{
    return _messages->Get(_id, threadId);
}

void NetThread::Send(unsigned int id, ThreadMessage* message)
{
    _messages->Send(id, message, _id);
}

ThreadQueue::ThreadQueue()
{

}

ThreadQueue::~ThreadQueue()
{
    for(auto mapIt = _queue.begin(); mapIt != _queue.end(); ++mapIt)
    {
        while(!(*mapIt).second.empty())
        {
            delete (*mapIt).second.front();
            (*mapIt).second.pop_front();
        }
    }
}

ThreadMessage* ThreadQueue::Get(unsigned int id)
{
    _mutex.lock();

    ThreadMessage* message = NULL;

    if(!_queue[id].empty())
    {
        message = _queue[id].front();
        _queue[id].pop_front();
    }

    _mutex.unlock();

    return message;
}
#include <iostream>
ThreadMessage* ThreadQueue::Get(unsigned int receiverId, unsigned int senderId)
{
    _mutex.lock();

    ThreadMessage* message = NULL;

    for(auto qeueuIt = _queue[receiverId].begin(); qeueuIt != _queue[receiverId].end(); ++qeueuIt)
    {
        if((*qeueuIt)->GetSender() == senderId)
        {
            message = *qeueuIt;
            _queue[receiverId].erase(qeueuIt);
            break;
        }
    }

    _mutex.unlock();

    return message;
}

void ThreadQueue::Send(unsigned int receiverId, ThreadMessage* message, unsigned int senderId)
{
    message->SetSender(senderId);

    _mutex.lock();

    _queue[receiverId].push_back(message);

    _mutex.unlock();
}

ThreadQueue* NetThread::CreateMessageContainer()
{
   return new ThreadQueue();
}

NetThread::~NetThread()
{
    _stop.store(true);

    if(_thread != NULL)
        delete _thread;
}

ThreadMessage::ThreadMessage(int type)
{
    _type = type;
}

string& ThreadMessage::GetData(int index)
{
    return _data[index];
}

void ThreadMessage::SetData(const char *str)
{
    _data.push_back(string(str));
}

int ThreadMessage::GetType()
{
    return _type;
}

unsigned int ThreadMessage::GetSender()
{
    return _sender;
}

void ThreadMessage::SetSender(unsigned int sender)
{
    _sender = sender;
}

