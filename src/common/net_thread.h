//! Soubor net_thread.h
/*! Trida pro praci s vlakny.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef NET_THREAD_H
#define NET_THREAD_H

#include <mutex>
#include <deque>
#include <map>
#include <vector>
#include <string>
#include <thread>
#include <chrono>
#include <atomic>

//! Trida ThreadMessage.
/*!
  ThreadMessage slouzi jako zprava mezi vlakny. Muze obsahovat data ktere,
  zasila odesilatel prijemci.
*/
class ThreadMessage
{
    //! Privatni polozky
    private:

        //!< Vektor dat, ktere budou odeslany prijmeci.
        std::vector<std::string> _data;

        //!< Typ zpravy.
        int _type;

        //!< Identifikator odesilatele zpravy.
        unsigned int _sender;

    //! Verejne polozky
    public:

        //!< Konstruktor.
        ThreadMessage(int type);

        /*!
          \sa GetData()
          \param index Index pro ziskani dat.
          \return Navraci retezec reprezentujici data.
        */
        std::string& GetData(int index);

        /*!
          \sa SetData(const char *str)
          \param str Retezec, ktery reprezentuje data.
        */
        void SetData(const char *str);

        /*!
          \sa GetSender()
          \return Navraci idetifikator odesilatele.
        */
        unsigned int GetSender();

        /*!
          \sa SetSender(unsigned int sender)
          \param sender Identifikateoru odesilatele.
        */
        void SetSender(unsigned int sender);

        /*!
          \sa GetType()
          \return Vraci typ zpravy.
        */
        int GetType();
};

//! Trida ThreadQueue.
/*!
  ThreadQueue tato trida implementuje frontu mezi vlakny.
*/
class ThreadQueue
{
    //! Privatni polozky
    private:

        //!< Mapa, ktera seskupuje fronty pro jednotlive vlakna, podle identifikatoru vlakna.
        std::map<unsigned int, std::deque<ThreadMessage*>> _queue;

        //!< Mutex nad mapou, zajistuje synchronizaci mezi vlakny.
        std::mutex _mutex;

    //! Verejne polozky
    public:

        //!< Konstruktor.
        ThreadQueue();

        //!< Destruktor.
        virtual ~ThreadQueue();

        /*!
          \sa Get(unsigned id)
          \param id Identifikateoru prijemce.
          \return Navraci zpravu pro prijmence, pokud je fronta prazda navraci NULL.
        */
        ThreadMessage* Get(unsigned id);

        /*!
          \sa Get(unsigned int receiverId, unsigned int senderId)
          \param receiverId Identifikateoru prijemce.
          \param senderId Identifikateoru odesilatele.
          \return Navraci zpravu prvni zaslanou zpravou danym odesilatelem (sednerId),
          pokud nenalezne zpravu od daneho odesilatele navraci NULL.
        */
        ThreadMessage* Get(unsigned int receiverId, unsigned int senderId);

        /*!
          \sa Send(unsigned int receiverId, ThreadMessage* message, unsigned int senderId = 0)
          \param receiverId Identifikateoru prijemce.
          \param message Zprava.
          \param senderId Nepovinny parametr, indetifikator odesilatele.
        */
        void Send(unsigned int receiverId, ThreadMessage* message, unsigned int senderId = 0);
};

//! Trida NetThread.
/*!
  NetThread implementuje vlakno.
*/
class NetThread
{
    //! Privatni polozky
    private:

        //!< Fronta zprav pro synchronizaci vlaken.
        ThreadQueue *_messages;

        //!< Vlakno.
        std::thread *_thread;

    //! Chranene polozky
    protected:

        //!< Virtualni metoda, tato metoda je vykonavana vlaknem.
        virtual void Execute();

        //!< Indetifikator vlakna.
        unsigned int _id;

        //!< Atomicka promenna pro rizeni stavu vlakna, true vlakno je zastaveno, false vlakno bezi.
        std::atomic<bool> _stop;

        /*!
          \sa Get()
          \return Navraci zpravu pro prijmence, pokud je fronta prazda navraci NULL.
        */
        ThreadMessage* Get();

        //!< Vyhleda zpravu podle odesilatele(threadId) ve fronte zprav a odebere zpravu z fronty.
        /*!
          \sa Get()
          \param threadId Identifikator odesilatele.
          \return Navraci zpravu, pri neuspechu navraci NULL.
        */
        ThreadMessage* Get(unsigned int threadId);

        //!< Vyhleda zpravu podle odesilatele(threadId) ve fronte zprav a odebere zpravu z fronty.
        /*!
          \sa Send()
          \param id Identifikator prijemce.
          \param message Zprava.
        */
        void Send(unsigned int id, ThreadMessage* message);

    //! Verejne polozky
    public:

        //!< Konstruktor.
        /*!
          \sa NetThread()
          \param messages Fronta zprav pro synchronizaci vlaken.
          \param id identifikator daneho vlakna.
        */
        NetThread(ThreadQueue *_messages, unsigned int id);

        //!< Destruktor
        virtual ~NetThread();

        //!< Zastavi chod vlakna reps. nastavi stav stop a vlakno se korektne ukonci.
        void Stop();

        //!< Spusti provadeni vlakna.
        void Start();

        /*!
          \sa GetID()
          \return Navraci indetifikator vlakna.
        */
        unsigned int GetID();

        //!< Tovarni staticka metoda pro vytvareni front.
        /*!
          \sa CreateMessageContainer()
          \return Navraci nove vytvorenou frontu.
        */
        static ThreadQueue* CreateMessageContainer();
};


#endif // NET_THREAD_H
