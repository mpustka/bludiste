//! Soubor finish.cpp
/*! Cil hry.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "finish.h"

using namespace pugi;

Finish::Finish(Tile *tile, xml_node &object) : Object(tile, object)
{
    _type = FINISH;
    LoadFromXML(object);
}

void Finish::LoadFromXML(xml_node &object)
{

}


