//! Soubor finish.h
/*! Cil hry.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef __FINISH_H__
#define __FINISH_H__

#include <vector>
#include <string>

#include "object.h"
#include "../pugi/pugixml.hpp"

class Finish : public Object
{
	private:
	public:

        Finish(Tile *tile, pugi::xml_node &object);
        void LoadFromXML(pugi::xml_node &object);
};

#endif
