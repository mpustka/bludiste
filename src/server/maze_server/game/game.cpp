//! Soubor game.cpp
/*! Soubor obsahuje definice metod tridy Game
    Game reprezentuje hru, ktera je vytvorena hracem. Kazda hra
    je by mela bezet ve vlastnim vlakne, z casovych duvodu je provaden
    herni krok v ramci protokolu. Pri kazdem tomto hernim kroku se vyhodnoti
    vsechny dulezite akce a zmeny jsou poslany jednotlivym klientum.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "game.h"

using namespace std;

//! Inicializace staticky promennych tridy game.
vector<string> Game::_maps;         //! Je nutne inicializovat take vektor retezcu pro uchovani map.
string Game::_pathMaps = "./";      //! Implicitni cesta je nastaven na lokalni adresar.

//! Konstruktor tridy Game, vola, konstruktor sve nadtridy NetThread a nastavuje cas herniho kroku.
Game::Game(ThreadQueue* messages,
            unsigned int id,
            float speed)
    : NetThread(messages, id)
{
    _duration =  std::chrono::milliseconds((int) (1000 * speed));// dura(1000 * speed);
}

Game::~Game() { }

//! Metoda pro pridani noveho hrace do hry.
bool Game::AddPlayer(int id)
{
    if(_firstId == 0)
        _firstId = id;

    for(auto it = _players.begin(); it != _players.end(); ++it)
    {
        if(!(*it)->IsConnected() && (*it)->GetState() != PlayerState::KILLED)
        {
            (*it)->Connect(id, _firstId);
            SendMapData(id);
            return true;
        }
    }

    return false;
}

//! Metoda pro kontrolu zda se dany klient nachazi ve hre.
bool Game::IsInGame(int id)
{
    for(auto it = _players.begin(); it != _players.end(); ++it)
    {
        if(!(*it)->GetID() == id)
            return true;
    }

    return false;
}

//! Getter stavu hry.
GameState Game::GetState()
{
    return _gameState;
}

//! Metoda pro nastaveni adresare map.
void Game::SetMapDirectory(const string &path)
{
    Game::_pathMaps = path;
}

//! Metoda pro nastaveni seznamu map pro hry. lze jej menit za behu serveru.
void Game::SetListMaps(const string &path)
{
    string line;
    ifstream listMaps(path.c_str());

    if(listMaps.is_open())
    {
        while(getline(listMaps, line))
            Game::_maps.push_back(line);

        listMaps.close();
    }else{
        throw ios_base::failure("Map hasn't been loaded.");
    }
}

//! Metoda realizuje herni smycku.
/*! hra je ukoncena dosazenim cile nebo vnejsim zasahem protoklu do stavu hry.
 */
void Game::Execute()
{
    while (!_stop.load())
    {
        //! Odstartujeme mereni casu pro herni cyklus.
        auto begin = std::chrono::high_resolution_clock::now();

        //! Vyresetujeme display abychom posilali spravna data hracum
        _maze.ResetDisplay();

        //! Nastavime smer strazcu
        for(auto it = _guards.begin(); it != _guards.end(); ++it)
        {
            Direction dir = static_cast<Direction>(GenerateRandomNumber(4));
            PlayerState state = PlayerState::GO;//static_cast<PlayerState>(GenerateRandomNumber(2));

            (*it)->SetState(state);
            (*it)->SetDirection(dir);
        }

        //! Pohneme vsemi hraci
        for(auto it = _players.begin(); it != _players.end(); ++it)
        {
            if((*it)->IsConnected())
            {
                if((*it)->Move())
                {
                    _gameState = GameState::GAMEOVER;
                    _stop.store(true);
                    SendGameResults((*it)->GetOrderOfGame());
                }
            }
        }

        //! Pohneme se vsemi strazci
        for(auto it = _guards.begin(); it != _guards.end(); ++it)
            (*it)->Move();
        _maze.UpdateDisplay();
        SendDynamicData();


        //! Pocitani casu od zacatku herniho cyklu usnadni casovou synchronizaci hry
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::milliseconds elapsed(std::chrono::duration_cast<std::chrono::milliseconds>(end-begin).count());

        //! Pokud je uplnuly cas vetsi nez doba po kterou se ma uspat, pokracujeme dalsim cyklem
        if (elapsed > _duration)
            continue;

        //! Jinak upsime na puvodni dobu - cas herniho cyklu
        std::this_thread::sleep_for(_duration - elapsed);
    }

    for (auto ch = _players.begin(); ch != _players.end(); ++ch)
    {
        (*ch)->Disconnect();
    }
}

//! Metoda realizuje zasilani vysledku hry.
void Game::SendGameResults(int id)
{
    ThreadMessage* message;

    //! Nacteme vsechna potrebna data.
    std::stringstream ss;
    ss.precision(3);

    ss << "150 GameOver winner: player " << id << ", time in game:";
    int i = 0;
    for (auto ch = _players.begin(); ch != _players.end(); ++ch)
    {
        if ((*ch)->IsConnected())
            ss << " p" << ++i << ": " << (*ch)->GetTimeInGame() << "sec";
    }

    ss << DTERM;
    std::string gameData = ss.str();

    std::cout << gameData;
    for (auto ch = _players.begin(); ch != _players.end(); ++ch)
    {
        message = new ThreadMessage(0);
        message->SetData(gameData.c_str());
        Send((*ch)->GetID(), message);
    }
}

//! Metoda realizuje zasilani informace o mape konkretnimu hraci.
void Game::SendMapData(int id)
{
    ThreadMessage *message = new ThreadMessage(0);
    Display* display = _maze.GetDisplay();
    display->Lock();                       //!< Uzamkneme display.

    //! Nacteme vsechna potrebna data z displeje a ulozime ve formatu pro klienty.
    stringstream ss;
    for(auto it = display->BeginStatic(); it != display->EndStatic(); ++it)
        ss << DBOX << (int) it->second->data << " " << it->second->x << " " << it->second->y << DTERM;

    display->Unlock();

    message->SetData(ss.str().c_str());
    Send(id, message);
}

//! Metoda realizuje zasilani informace o mape vsem hracum dane hry.
void Game::SendDynamicData()
{
    ThreadMessage* message;
    Display* display = _maze.GetDisplay();
    display->Lock();                       //!< Uzamkneme display.

    //! Nacteme vsechna potrebna data z displeje a ulozime ve formatu pro klienty.
    std::stringstream ss;
    for(auto it = display->Begin(); it != display->End(); ++it)
        ss << DBOX << (int) it->second->data << " " << it->second->x << " " << it->second->y << DTERM;

    display->Unlock();

    std::string mapData = ss.str();
    for (auto ch = _players.begin(); ch != _players.end(); ++ch)
    {
        if ((*ch)->IsConnected() == false)
            continue;

        //! Zprava se naplni zpravou konkretniho stavu protokolu
        message = new ThreadMessage(0);
        message->SetData(mapData.c_str());
        //! A je zarazena do fronty
        Send((*ch)->GetID(), message);
    }
}

//! Metoda pro nacteni mapy do hry.
void Game::LoadMap(const string &name)
{
    _maze.LoadFromXML(Game::_pathMaps + "/" + name);

    for(auto it = _maze.GetGuards()->begin(); it != _maze.GetGuards()->end(); ++it)
       _guards.push_back((Guard*)*it);

    for(auto it = _maze.GetPlayers()->begin(); it != _maze.GetPlayers()->end(); ++it)
       _players.push_back((Player*)*it);

    _maze.LoadStaticData();

}

//! Metoda pro ziskani jmen dostupnych map nad hrou
vector<string> *Game::GetMaps()
{
    return &_maps;
}

//! Metoda pro ziskani displaye soucasneho stavu hry
Display *Game::GetDisplay()
{
    return _maze.GetDisplay();
}

//! Metoda pro vraceni vektoru hracu v dane hre
std::vector<Player*>* Game::GetPlayers()
{
    return &_players;
}

//! Metoda pro nahodne generovani cisel
int Game::GenerateRandomNumber(int max)
{
    static int factor = 6;
    factor++;
    srand(time(NULL));
    return (rand() + factor) % max;
}

//! Metoda pro ziskani aktialne hrane mapy
Map *Game::GetMap()
{
    return &_maze;
}
