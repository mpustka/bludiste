//! Soubor game.h
/*! Hlavickovy soubor obsahujici definici tridy Game
    Game reprezentuje hru, ktera je vytvorena hracem. Kazda hra
    je by mela bezet ve vlastnim vlakne, z casovych duvodu je provaden
    herni krok v ramci protokolu. Pri kazdem tomto hernim kroku se vyhodnoti
    vsechny dulezite akce a zmeny jsou poslany jednotlivym klientum.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */


#ifndef __GAME_H__
#define __GAME_H__

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>

#include "map.h"
#include "player.h"
#include "guard.h"
#include "../../../common/net_thread.h"

#define DBOX "130 "
#define DTERM "\r\n"

typedef enum
{
    RUNNING = 0,
    GAMEOVER = 1

}GameState;

class Game : public NetThread
{
	private:

		Map _maze;
        std::vector<int> _playerIDs;
		GameState _gameState = GameState::RUNNING;
		std::chrono::milliseconds _duration;

        static std::vector<std::string> _maps;
        static std::string _pathMaps;

        std::vector<Player*> _players;
        std::vector<Guard*> _guards;

        int _firstId = 0;

	public:
	    static const int STATE_OK   = 0;
        static const int STATE_RUN  = 1;

        Game(ThreadQueue* messages, unsigned int id, float speed);
        virtual ~Game();

        GameState GetState();

        bool AddPlayer(int id);       //! Prida do hry hrace s konretnim id        bool IsInGame(int id);
        bool IsInGame(int id);
        void SendMapData(int id);
        void SendGameResults(int id);
        void SendDynamicData();

        static void SetMapDirectory(const std::string &path);
        static void SetListMaps(const std::string &path);
        static std::vector<std::string> *GetMaps();
        void LoadMap(const std::string &name);

        virtual void Execute();

        Display *GetDisplay();
        Map *GetMap();
        std::vector<Player*>* GetPlayers();
        static int GenerateRandomNumber(int max);
};

#endif
