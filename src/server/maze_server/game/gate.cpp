//! Soubor gate.cpp
/*! Brana na mape.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "gate.h"
#include <cstring>
#include "../pugi/pugixml.hpp"

using namespace pugi;

Gate::Gate(Tile *tile, xml_node &object) : Object(tile, object)
{
    _opened = true;
    _type = GATE;
    LoadFromXML(object);
}

void Gate::LoadFromXML(xml_node &object)
{
    for (auto it = object.begin(); it != object.end(); ++it)
    {
        //TODO: udelat vyjimky, pokud je mapa neni validini
        if(strcmp(it->name(), "key") == 0)
        {
           Key *key = new Key(_tile, *it);
           _keys.push_back(key);
        }
    }

     _opened = object.attribute("opened").as_bool();
}

bool Gate::IsOpened()
{
    return _opened.load();
}

void Gate::Open()
{
    _opened.store(true);
}

int Gate::GetIdKey()
{
    if(!_keys.empty())
        return _keys[0]->GetId();

    return -1;
}

Gate::~Gate()
{
    while(!_keys.empty())
    {
       delete _keys.back();
       _keys.pop_back();
    }
}


