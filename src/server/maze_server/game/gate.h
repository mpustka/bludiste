//! Soubor gate.h
/*! Brana na mape.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef __GATE_H__
#define __GATE_H__

#include <vector>
#include <string>
#include <atomic>

#include "object.h"
#include "key.h"

class Gate : public Object
{
	private:

        std::atomic<bool> _opened;
		std::vector<Key*> _keys;

	public:

        Gate(Tile *tile, pugi::xml_node &object);
        virtual ~Gate();

		bool IsOpened();
		void Open();

        void LoadFromXML(pugi::xml_node &object);

        int GetIdKey();
};

#endif
