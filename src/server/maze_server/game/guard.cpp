//! Soubor guard.cpp
/*! Hlidac na mape, take zakladni objekt z ktere ho dedi hrac.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "guard.h"
#include "gate.h"
#include "player.h"

using namespace pugi;
using namespace std;

Guard::Guard(Tile *tile, xml_node &object) : Object(tile, object)
{
    _type = GUARD;
    _state = PlayerState::STOP;

    LoadFromXML(object);
}

void Guard::LoadFromXML(xml_node &object)
{
    string dir(object.attribute("direction").as_string());

    if(dir == "right")
    {
        _direction = Direction::RIGHT;
    }else if(dir == "left"){
        _direction = Direction::LEFT;
    }else if(dir == "top"){
        _direction = Direction::TOP;
    }else if(dir == "bottom"){
        _direction = Direction::BOTTOM;
    }
}

void Guard::SetDirection(Direction dir)
{
    _direction = dir;
}

Direction Guard::GetDirection()
{
    return _direction;
}

PlayerState Guard::GetState()
{
    return _state;
}

void Guard::SetState(PlayerState state)
{
    _state = state;
}

Tile* Guard::GetNextPosition()
{
    int x = _tile->GetPosition().X;
    int y = _tile->GetPosition().Y;

    Tile *newTile = NULL;

    switch(_direction)
    {
        case Direction::TOP: y--; break;
        case Direction::RIGHT: x++; break;
        case Direction::LEFT: x--; break;
        case Direction::BOTTOM: y++; break;
    }

    newTile = _tile->GetMap()->GetTile(x, y);

    return newTile;
}
#include <iostream>
using namespace std;
//! Zde je logika ovladani
bool Guard::Move()
{
    _moved = !_moved;

    if(_state != PlayerState::GO)
        return false;

    Tile *newTile = GetNextPosition();

    if(newTile == NULL)
    {
        _state = PlayerState::STOP;
    }else{

        Object *obj = newTile->FrontInactive();
        Guard *objAct = static_cast<Guard*>(newTile->Front());

        //! Kontola kolize se strazcem, pripadne pusteni vsech klicu na zem
        if(objAct != NULL && _type == TypeObject::GUARD && objAct->GetType() == TypeObject::PLAYER)
        {
            newTile->Pop();

            Player *player = static_cast<Player*>(objAct);
            std::vector<Key*> *keys = player->GetKeys();

            while(keys->empty() == false)
            {
                newTile->Push(keys->back());
                keys->pop_back();
            }

            objAct->SetState(PlayerState::KILLED);
            //_state = PlayerState::STOP;

        }else if(objAct != NULL && _type == TypeObject::PLAYER && objAct->GetType() == TypeObject::GUARD){

            Player *player = static_cast<Player*>(this);
            _tile->Pop();

            std::vector<Key*> *keys = player->GetKeys();

            while(keys->empty() == false)
            {
                _tile->Push(keys->back());
                keys->pop_back();
            }

            SetState(PlayerState::KILLED);
            //_state = PlayerState::STOP;

        }else if(objAct != NULL && (objAct->GetState() != PlayerState::GO || objAct->Moved() == _moved)){

            if(!objAct->GetType() == TypeObject::PLAYER || ((Player*)objAct)->IsConnected())
                _state = PlayerState::STOP;

        }else if(newTile->HasObject(TypeObject::WALL)){
            _state = PlayerState::STOP;
        }else if(newTile->HasObject(TypeObject::GATE)){
            Gate *gate = static_cast<Gate*>(obj);

            if(!gate->IsOpened())
                _state = PlayerState::STOP;

        }else if(newTile->HasObject(TypeObject::FINISH) && GetType() == TypeObject::PLAYER){

            Player *player = static_cast<Player*>(this);

            _tile->Pop();
            _state = PlayerState::STOP;

            return true;
        }
    }

    if(_state == PlayerState::GO)
    {
        _tile->Pop();
        newTile->Push(this);
        _tile = newTile;
    }

    return false;
}

bool Guard::Moved()
{
    return _moved;
}
