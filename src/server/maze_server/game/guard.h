//! Soubor guard.h
/*! Hlidac na mape, take zakladni objekt z ktere ho dedi hrac.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef __GUARD_H__
#define __GUARD_H__

#include <vector>
#include <string>

#include "object.h"
#include "../pugi/pugixml.hpp"

class Guard : public Object
{
	protected:

        Direction _direction;
        PlayerState _state;

        bool _moved = false;

	public:

        Guard(Tile *tile, pugi::xml_node &object);

        virtual void LoadFromXML(pugi::xml_node &object);

        void SetDirection(Direction dir);
        Direction GetDirection();

        PlayerState GetState();
        void SetState(PlayerState state);

        bool Move();
        bool Moved();

        Tile* GetNextPosition();
};

#endif
