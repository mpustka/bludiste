//! Soubor key.cpp
/*! Klic, kterym muze uzivatel odemykat branu.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "key.h"
#include "../pugi/pugixml.hpp"

using namespace pugi;

Key::Key(Tile *tile, xml_node &object) : Object(tile, object)
{
    LoadFromXML(object);
    _type = KEY;
}

Key::~Key()
{

}

void Key::LoadFromXML(xml_node &object)
{
    _id = object.attribute("id").as_int();
}

int Key::GetId()
{
    return _id;
}
