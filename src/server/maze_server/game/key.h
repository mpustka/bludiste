//! Soubor key.h
/*! Klic, kterym muze uzivatel odemykat branu.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef __KEY_H__
#define __KEY_H__

#include <vector>
#include <string>

#include "object.h"
#include "../pugi/pugixml.hpp"

//class Object;

class Key : public Object
{
    private:

        int _id;

	public:

        Key(Tile *tile, pugi::xml_node &object);
        virtual ~Key();

        void LoadFromXML(pugi::xml_node &object);
        int GetId();
};

#endif
