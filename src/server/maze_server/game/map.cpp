//! Soubor map.cpp
/*! Trida drizici informace o cele mape.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "map.h"
#include "key.h"

#include <iostream>
#include <fstream>

using namespace pugi;
using namespace std;

Map::Map()
{

}

void Map::LoadFromXML(const string &path)
{
    xml_document doc;
    xml_parse_result result = doc.load_file(path.c_str());

    if(result.status != status_ok)
        throw ios_base::failure("Map hasn't been loaded.");

    xml_node root = doc.first_child();

    //parameters of maps
    _sizeX = root.attribute("sizeX").as_int();
    _sizeY = root.attribute("sizeY").as_int();

    for (xml_node_iterator it = root.begin(); it != root.end(); ++it)
    {
        Tile *tile = new Tile(this, *it);
        _tiles.push_back(tile);

        if(tile->HasObject(TypeObject::GUARD))
        {
            _guards.push_back(tile->Front());
        }else if(tile->HasObject(TypeObject::PLAYER)){
            _players.push_back(tile->Front());
        }
    }
}

void Map::ResetDisplay()
{
    _display.Reset();
}

Display *Map::GetDisplay()
{
    return &_display;
}

void Map::UpdateDisplay()
{
    for(auto it = _tiles.begin(); it != _tiles.end(); ++it)
    {
        Tile *tile = *it;
        Position pos = tile->GetPosition();
        char data = tile->DrawDisplay();

        if(data != 0 && data != DIS_WALL && data != DIS_FINISH)
        {
            _display.Set(pos.X, pos.Y, data);

        }
    }
}

void Map::LoadStaticData()
{
    for(auto it = _tiles.begin(); it != _tiles.end(); ++it)
    {
        Tile *tile = *it;
        Position pos = tile->GetPosition();
        char data = tile->DrawDisplay();

        if(data == DIS_WALL || data == DIS_FINISH)
            _display.SetStatic(pos.X, pos.Y, data);
    }
}

Tile* Map::GetTile(int x, int y)
{
    Tile *tile = NULL;

    if(x >= _sizeX || y >= _sizeY || y < 0 || x < 0)
        return NULL;

    for(auto it = _tiles.begin(); it != _tiles.end(); ++it)
    {
        Position position = (*it)->GetPosition();

        if(position.X == x && position.Y == y)
        {
            tile = *it;
            break;
        }
    }

    if(tile == NULL)
    {
        tile = new Tile(this, x, y);
        _tiles.push_back(tile);

        return tile;
    }else{
        return tile;
    }
}


vector<Object*> *Map::GetPlayers()
{
    return &_players;
}

vector<Object*> *Map::GetGuards()
{
    return &_guards;
}

/*Key * Map::NewKey(xml_node &key)
{
    Key *temp = new Key(this, key);
    Key *existKey = NULL;

    if((existKey = GetKeyById(temp->GetId())) == NULL)
    {
        _keys.push_back(temp);
        return temp;
    }

    return existKey;
}

Key * Map::GetKeyById(int id)
{
    for (auto it = _keys.begin(); it != _keys.end(); ++it)
    {
        if((*it)->GetId() == id)
            return *it;
    }

    return NULL;
}*/

Map::~Map()
{
    while(!_tiles.empty())
    {
       delete _tiles.back();
       _tiles.pop_back();
    }

/*    while(!_keys.empty())
    {
       delete _keys.back();
       _keys.pop_back();
    }*/
}

int Map::GetSizeX()
{
    return _sizeX;
}
int Map::GetSizeY()
{
    return _sizeY;
}
