//! Soubor map.h
/*! Trida drizici informace o cele mape.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef __MAP_H__
#define __MAP_H__

#include <vector>
#include <string>

#include "object.h"
#include "tile.h"
#include "structs.h"

#include "../pugi/pugixml.hpp"
#include "../../../common/display.h"


class Tile;
class Object;

class Map
{
	private:

		int _sizeX;
		int _sizeY;

		std::string _name;
        std::vector<Tile*> _tiles;

        std::vector<Object*> _players;
        std::vector<Object*> _guards;

        Display _display;

	public:

        Map();
        virtual ~Map();

        void LoadFromXML(const std::string &path);

        Tile* GetTile(int x, int y);
        void LoadStaticData();

       /* Key *NewKey(pugi::xml_node &key);
        Key *GetKeyById(int id);*/
        void UpdateDisplay();
        Display *GetDisplay();
        void ResetDisplay();

        int GetSizeX();
        int GetSizeY();

       std::vector<Object*> *GetPlayers();
       std::vector<Object*> *GetGuards();
};

#endif
