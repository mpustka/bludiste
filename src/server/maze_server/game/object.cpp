//! Soubor objekt.cpp
/*! Elementarni jednotka mapy, objekt ve smyslu hrac, zed, klic....

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "object.h"

using namespace pugi;

Object::Object(Tile *tile, xml_node &object)
{
    _tile = tile;
    _type = OTHER;
}

Object::~Object()
{
}

bool Object::ActiveObject()
{
    if(_type == PLAYER || _type == GUARD)
        return true;

    return false;
}

/*Map *Object::GetMap()
{
    return _map;
}*/

TypeObject Object::GetType()
{
    return _type;
}


