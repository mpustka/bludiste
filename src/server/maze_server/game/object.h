//! Soubor objekt.h
/*! Elementarni jednotka mapy, objekt ve smyslu hrac, zed, klic....

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <vector>

#include <string>

#include "../pugi/pugixml.hpp"
#include "tile.h"
#include "structs.h"

typedef enum
{
    LEFT = 0,
    RIGHT = 1,
    TOP = 2,
    BOTTOM = 3,
}Direction;

class Tile;

class Object
{
    private:


    protected:

        TypeObject _type;
        Tile *_tile;

	public:

        Object(Tile *_tile, pugi::xml_node &object);
        virtual ~Object();

        virtual void LoadFromXML(pugi::xml_node &object) = 0;

        TypeObject GetType();
        bool ActiveObject();
      //  Map *GetMap();
};

#endif
