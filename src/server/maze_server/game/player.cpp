//! Soubor player.h
/*! Soubor obsahujici definice metod tridy Player.
    Player reprezentuje postavu v bludisti. Kazda postava muze byt spojena
    s klientem ktery tuto postavu ovlada pomoci prikazu z konzole.
    Vse je napsano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste

 */

#include "player.h"
#include "gate.h"

using namespace pugi;
using namespace std;

Player::Player(Tile *tile, xml_node &object) : Guard(tile, object)
{
    _type = PLAYER;
    _connected.store(false);
    _spawn = tile;

    LoadFromXML(object);
    _id = -1;
}

void Player::LoadFromXML(xml_node &object)
{
    string dir(object.attribute("direction").as_string());

    if(dir == "right")
    {
        _direction = Direction::RIGHT;
    }else if(dir == "left"){
        _direction = Direction::LEFT;
    }else if(dir == "top"){
        _direction = Direction::TOP;
    }else if(dir == "bottom"){
        _direction = Direction::BOTTOM;
    }
}


bool Player::IsConnected()
{
    return _connected.load();

}

void Player::Disconnect()
{
    _connected.store(false);
}

void Player::Connect(int id, int offset)
{
    _id = id;
    _offsetId = offset;
    _connected.store(true);
    Player::playerMap[id] = this;
    _stamp = std::chrono::system_clock::now();
}

int Player::GetID()
{
    return _id;
}

void Player::AddKey(Key* key)
{
    _keys.push_back(key);
}


std::vector<Key*>* Player::GetKeys()
{
    return &_keys;
}


bool Player::Unlock(int id)
{
    for(auto it = _keys.begin(); it != _keys.end(); ++it)
    {
        if(id == (*it)->GetId())
        {
            _keys.erase(it);
            return true;
        }
    }

    return false;
}

bool Player::Take()
{
    Tile *newTile = GetNextPosition();
    bool opened = false;

    if(newTile != NULL)
    {
        Object *obj = newTile->FrontInactive();

        if(obj != NULL && obj->GetType() == TypeObject::KEY)
        {
            this->AddKey(static_cast<Key*>(obj));
            newTile->Pop(obj);
            opened = true;
        }
    }

    if(_tile != NULL)
    {
        Object *obj = _tile->FrontInactive();

        if(obj != NULL && obj->GetType() == TypeObject::KEY)
        {
            this->AddKey(static_cast<Key*>(obj));
            _tile->Pop(obj);
            opened = true;
        }
    }

    return opened;
}

bool Player::Open()
{
    Tile *newTile = GetNextPosition();

    if(newTile == NULL)
        return false;

    Object *obj = newTile->FrontInactive();

    if(obj == NULL || obj->GetType() != TypeObject::GATE)
        return false;

    Gate *gate = static_cast<Gate*>(obj);
    int idKey = gate->GetIdKey();

    if(idKey != -1)
    {
        if(this->Unlock(idKey))
        {
            _state = PlayerState::STOP;
            gate->Open();
            return true;
        }
    }
}

bool Player::Kill()
{
    Tile *newTile = GetNextPosition();

    if(newTile == NULL)
        return false;

    Object *obj = newTile->Front();

    if(obj == NULL || obj->GetType() != TypeObject::PLAYER)
        return false;

    Player *player= static_cast<Player*>(obj);
    std::vector<Key*> *keys = player->GetKeys();

    newTile->Pop(obj);
    while(keys->empty() == false)
    {
        newTile->Push(keys->back());
        keys->pop_back();
    }

    player->Respawn();
    player->SetState(PlayerState::STOP);
    //player->Respawn();
    return true;
}

float Player::GetTimeInGame()
{
    std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
    std::chrono::duration<float> elapsed = end-_stamp;
    return elapsed.count();
}

Player* Player::GetPlayer(int id)
{
    return playerMap[id];
}

void Player::Respawn()
{
    _tile = _spawn;
    _spawn->Push(this);
}

Player::PlayerMap Player::createPlayerMap()
{
    PlayerMap ret;
    // populate ret
    return ret;
}

int Player::GetOrderOfGame()
{
    return (_id - _offsetId) + 1;
}

Player::PlayerMap Player::playerMap(Player::createPlayerMap());

