//! Soubor player.h
/*! Hlavickovy soubor obsahujici definici tridy Player
    Player reprezentuje postavu v bludisti. Kazda postava muze byt spojena
    s klientem ktery tuto postavu ovlada pomoci prikazu z konzole.
    Vse je napsano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <vector>
#include <string>
#include <atomic>

#include "guard.h"
#include "key.h"
#include "../pugi/pugixml.hpp"

class Player : public Guard
{
    private:

        int _id;
        int _offsetId = 0;
        std::atomic<bool> _connected;
        std::chrono::time_point<std::chrono::system_clock> _stamp;
        Tile* _spawn;
        std::vector<Key*> _keys;

	public:

        typedef std::map<int, Player*> PlayerMap;
        static PlayerMap createPlayerMap();
        static PlayerMap playerMap;

        Player(Tile *tile, pugi::xml_node &object);
        void LoadFromXML(pugi::xml_node &object);

        int GetID();
        int GetOrderOfGame();

        bool IsConnected();
        void Connect(int id, int offset);
        void Disconnect();
        void Respawn();

        void AddKey(Key* key);
        bool Unlock(int id);

        float GetTimeInGame();
        static Player* GetPlayer(int id);

        std::vector<Key*>* GetKeys();
        bool Take();
        bool Open();
        bool Kill();
};

#endif
