//! Soubor struct.h
/*! Definice struktur potrebna pro praci s mapou.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef STRUCTS_H_INCLUDED
#define STRUCTS_H_INCLUDED

typedef enum
{
    OTHER = 0,
    PLAYER = 1,
    GUARD = 2,
    GATE = 3,
    FINISH = 4,
    KEY = 5,
    WALL = 6,
}TypeObject;

typedef enum
{
    GO = 0,
    STOP = 1,
    KILLED = 2
}PlayerState;


#endif // STRUCTS_H_INCLUDED
