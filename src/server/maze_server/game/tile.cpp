//! Soubor tile.cpp
/*! Obsahuje definici metod tridy tile. Vse je psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "tile.h"

//! Includy jsou vlozeny zde z duvodu rekurzivnich zavislosti
#include "wall.h"
#include "guard.h"
#include "gate.h"
#include "finish.h"
#include "player.h"
#include "key.h"

using namespace pugi;
using namespace std;

Tile::Tile(Map *map, xml_node &tile)
{
    _map = map;
    LoadFromXML(tile);
}

Tile::Tile(Map *map, int x, int y)
{
    _map = map;
    _x = x;
    _y = y;
}

//! Strucny koment
/*! podrobnejsi popis
 *
 */
void Tile::LoadFromXML(xml_node &tile)
{
    //parameters of tile
    _x = tile.attribute("x").as_int();
    _y = tile.attribute("y").as_int();

    for (xml_node_iterator it = tile.begin(); it != tile.end(); ++it)
    {
        Object *object = NULL;

        if(strcmp(it->name(), "wall") == 0)
        {
            object = new Wall(this, *it);
        }else if(strcmp(it->name(), "key") == 0){
            object = new Key(this,*it);
        }else if(strcmp(it->name(), "guard") == 0){
            object = new Guard(this,*it);
        }else if(strcmp(it->name(), "player") == 0){
            object = new Player(this,*it);
        }else if(strcmp(it->name(), "gate") == 0){
            object = new Gate(this,*it);
        }else if(strcmp(it->name(), "finish") == 0){
            object = new Finish(this,*it);
        }

        _objects.push_back(object);
    }
}

Position Tile::GetPosition()
{
  Position position;

  position.X = _x;
  position.Y = _y;

  return position;
}
#include <iostream>
char Tile::DrawDisplay()
{
  Object* object = Front();
  char data;

  if(object != NULL)
  {
      if(object->GetType() == TypeObject::PLAYER)
      {
        Player *player = static_cast<Player*>(object);

        if (player->IsConnected() != false)
        {
          switch(player->GetDirection())
          {
            case Direction::BOTTOM: data = DIS_PLAYER_BOTTOM; break;
            case Direction::LEFT: data = DIS_PLAYER_LEFT; break;
            case Direction::RIGHT: data = DIS_PLAYER_RIGHT; break;
            case Direction::TOP: data = DIS_PLAYER_UP; break;
          }

          char id = (char)player->GetOrderOfGame() << 4;
          data = data | id;

          return data;
        }
        else
          return 0;
      }else if(object->GetType() == TypeObject::GUARD){
          Guard *guard = static_cast<Guard*>(object);

          switch(guard->GetDirection())
          {
              case Direction::BOTTOM: data = DIS_GUARD_BOTTOM; break;
              case Direction::LEFT: data = DIS_GUARD_LEFT; break;
              case Direction::RIGHT: data = DIS_GUARD_RIGHT; break;
              case Direction::TOP: data = DIS_GUARD_UP; break;
          }

          return data;
      }
  }else if(!_objects.empty()){

     object = _objects[0];

     switch(object->GetType())
     {
        case KEY: data = DIS_KEY; break;
        case WALL: data = DIS_WALL; break;
        case FINISH: data = DIS_FINISH; break;
        case GATE: data = ((Gate*)object)->IsOpened() ? DIS_GATE_OPENED : DIS_GATE_CLOSED; break;
     }
  }

  return data;
}

Object* Tile::Front()
{
    for(auto it = _objects.begin(); it != _objects.end(); ++it)
    {
        if((*it)->ActiveObject())
            return *it;
    }

    return NULL;
}

bool Tile::HasObject(TypeObject type)
{
    for(auto it = _objects.begin(); it != _objects.end(); ++it)
    {
        if((*it)->GetType() == type)
            return true;
    }

    return false;
}

Object* Tile::FrontInactive()
{
    for(auto it = _objects.begin(); it != _objects.end(); ++it)
    {
        if(!(*it)->ActiveObject())
            return *it;
    }

    return NULL;
}

void Tile::Pop()
{
    for(auto it = _objects.begin(); it != _objects.end(); ++it)
    {
        if((*it)->ActiveObject())
        {
            _map->GetDisplay()->Set(_x, _y, 0);
            _objects.erase(it);
            break;
        }
    }
}

void Tile::Pop(Object *object)
{
    for(auto it = _objects.begin(); it != _objects.end(); ++it)
    {
        if((*it) == object)
        {
            _map->GetDisplay()->Set(_x, _y, 0);
            _objects.erase(it);
            break;
        }
    }
}

void Tile::Push(Object* object)
{
    _objects.insert(_objects.begin(), object);
}

Map *Tile::GetMap()
{
    return _map;
}

Tile::~Tile()
{
    while(!_objects.empty())
    {
        Object *object =_objects.back();

       /* if(object->GetType() != KEY)
          delete object;*/

       _objects.pop_back();
    }
}

