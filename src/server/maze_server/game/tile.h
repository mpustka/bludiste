//! Soubor tile.h
/*! Obsahuje definici tridy tile, ktera je zakladnim blokem na mape bludiste
    tile muze obsahovat nekolik jinych objektu.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __TILE_H__
#define __TILE_H__

#include <vector>
#include <string>
#include <cstring>

#include "map.h"

#include "object.h"
#include "structs.h"

#include "../pugi/pugixml.hpp"

typedef struct TPosition
{
	int X;
    int Y;
}Position;

class Map;
class Object;

class Tile
{
	private:

		int _x;
		int _y;

        Map *_map;

        std::vector<Object*> _objects;

	public:

        Tile(Map *map, pugi::xml_node &tile);
        Tile(Map *map, int x, int y);

        virtual ~Tile();

        void LoadFromXML(pugi::xml_node &tile);

		Position GetPosition();
		//void SetPosition(int x, int y);

		Object* Front();
		Object* FrontInactive();

		void Pop();
		void Pop(Object *object);

		void Push(Object *object);

		bool HasObject(TypeObject type);

        Map *GetMap();
        char DrawDisplay();
};

#endif
