//! Soubor wall.cpp
/*! Objekt typu zed, pres kterou hrac nemuze projit.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#include "wall.h"

using namespace pugi;

Wall::Wall(Tile *tile, xml_node &object) : Object(tile, object)
{
    _type = WALL;
    LoadFromXML(object);
}

void Wall::LoadFromXML(xml_node &object)
{

}


