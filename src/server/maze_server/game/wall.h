//! Soubor wall.h
/*! Objekt typu zed, pres kterou hrac nemuze projit.
    Psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */
#ifndef __WALL_H__
#define __WALL_H__

#include <vector>
#include <string>

#include "object.h"
#include "../pugi/pugixml.hpp"

class Wall : public Object
{
	private:
	public:

        Wall(Tile *tile, pugi::xml_node &object);
        void LoadFromXML(pugi::xml_node &object);
};

#endif
