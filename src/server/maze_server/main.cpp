//! Soubor main.cpp
/*! Hlavni smycka serveru. Vytvori instanci tridy server, a spusti nad nim
    protokol, ktery jej bude rezirovat. Hlavni smycka programu dale ceka na
    prikazy ze standardniho vstupu na externi prikazy.
    Prikaz QUIT ukonci chod serveru, terminuje pripojeni a zastavi vsechna
    vlakna. Pote se vypne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include <iostream>
#include <thread>

#include "game/game.h"
#include "server/server.h"
#include "server/maze_protocol.h"

const std::string default_port = "30000";
const int limit = 10;

using namespace std;
int main(int argc, char *argv[])
{
    string port;

    if (argc > 2)
        port = string(argv[1]);
    else
        port = default_port;

    Server server(port, limit);

    Game::SetMapDirectory("../../../examples/");
    Game::SetListMaps("../../../examples/list_maps.list");

    MazeProtocol prot(&server);
    cout << "Server is running." << endl;
    thread protocolThread(&MazeProtocol::HandleServer, prot);

    string command;
    while (server.GetState() == Server::STATE_OK)
    {
        cin >> command;
        if (command.compare("QUIT") == 0)
            break;
    }

    server.SetState(Server::STATE_STOP);
    protocolThread.join();
    cout << "Stopping server." << endl;
    return 0;
}

