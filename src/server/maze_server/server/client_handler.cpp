//! Soubor client_handler.cpp
/*! Soubor obsahuje definici metod tridy ClientHandler
    Vse je napsano rucne. Zajistuje komunikaci s klientem pomoci socketu.
    Handler bezi v samostatnem vlakne ktere komunikuje s protokolem serveru
    pomoci zprav Na zaklade sitove komunikace a vnitrniho stavu protokolu.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "client_handler.h"

//! Konstruktor tridy Server, zajisti vhodnou inicializaci
ClientHandler::ClientHandler(ThreadQueue* messages,
                             unsigned int id,
                             unsigned int socket)
    : NetThread(messages, id)
{
    _socket = socket;

    //! Nastaveni neblokujiciho cteni
    #ifdef _WIN32
        u_long iMode = 1;
        ioctlsocket(_socket, FIONBIO, &iMode);
    #else
        fcntl(_socket, F_SETFL, O_NONBLOCK);
    #endif
}

ClientHandler::~ClientHandler()
{
    closesocket(_socket);
}

void ClientHandler::Execute()
{
    std::chrono::milliseconds duration(4);
    ThreadMessage *message = NULL;
    std::string report;

    //! Zasleme protokolu uvodni spravu o pripojeni klienta
    message = new ThreadMessage(NEW_CLIENT_MESSAGE);
    message->SetData("NEW CLIENT");
    Send(DEFAULT_THREADS_CONTRLER_ID, message);

    while (!_stop.load())
    {
        report = GetLine();

        //! Prijata dat od klienta zasilame ridicimu protokolu pro zpracovani.
        if (report.empty() == false)
        {
            message = new ThreadMessage(GENERAL_MESSAGE);
            message->SetData(report.c_str());
            Send(DEFAULT_THREADS_CONTRLER_ID, message);
        }

        //! Pokud jsme obdrezli zpravu od protokolu, zasleme ji klientovi.
        if((message = Get()) != NULL)
        {
            report = message->GetData(0);
            send(_socket, report.c_str(), report.length(), 0);
            delete message;
        }

        //! Uspime vlakno na kratky okamzik aby nedoslo k zahlceni pozadavky.
        std::this_thread::sleep_for(duration);
    }
}

//! Meto dazjistuje cteni zprav se socketu po radcich
/*! Nejdrive je nactena zprava o velikosti bufferu, dale je nalezeno
    zakonceni radku a prvni cast je vracena zpet volajicimu. Zbytek je ulozen
    do bufferu ke kteremu budou nasledujici zpravy ze socketu pripojeny.
 */
std::string ClientHandler::GetLine()
{
    std::string line = "";
    char c;

    while (1)
    {
        if (recv(_socket, &c, 1, 0) <= 0)
            break;

        if (c == '\r')
        {
            recv(_socket, &c, 1, 0);
            break;
        }
        else if (c == '\n')
            break;

        line += c;
    }

    return line;                                //! Vracime precteny radek.
}
