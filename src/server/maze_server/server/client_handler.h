//! Soubor client_handler.h
/*! Hlavickovy soubor obsahujici definici tridy ClientHandler
    Vse je napsano rucne. Zajistuje komunikaci s klientem pomoci socketu.
    Handler bezi v samostatnem vlakne ktere komunikuje s protokolem serveru
    pomoci zprav Na zaklade sitove komunikace a vnitrniho stavu protokolu.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __CLIENTHANDLER_H__
#define __CLIENTHANDLER_H__

#include <iostream>
#include <string>
#include <vector>
#include "socket_portable.h"
#include "../../../common/net_thread.h"

#define DEFAULT_THREADS_CONTRLER_ID 0
#define GENERAL_MESSAGE 0
#define NEW_CLIENT_MESSAGE 1

//! Trida ClientHandler
/*! Zajistuje komunikaci s klientem pomoci socketu.
    Handler bezi v samostatnem vlakne ktere komunikuje s protokolem serveru
    pomoci zprav Na zaklade sitove komunikace a vnitrniho stavu protokolu.
 */
class ClientHandler : public NetThread
{
    private:
        //! Privatni atributy.
        int _socket;                //!< Ulozeni cisla socketu daneho klienta.

        //! Privatni metody.
        std::string GetLine();      //! Mmetoda zajistuje cteni ze socketu po radcich

    public:
        //! Verejne metody
        ClientHandler(ThreadQueue* messages, unsigned int id, unsigned int socket);
        virtual ~ClientHandler();

    protected:
        virtual void Execute();     //! Hlavni smycka ve ktere pobezi dane vlakno
};

#endif // __CLIENTHANDLER_H__
