//! Soubor maze_protocol.cpp
/*! Obsahuje implementace metod tridy MazeProtocol.
    Jedna se zejmena o rizeni chodu serveru a praci s klenty.
    Vse je psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "maze_protocol.h"

//! Naplneni statickych konstant tridy MazeProtocol
const std::string MazeProtocol::INIT_01     = "100 Games: ";
const std::string MazeProtocol::INIT_02     = "Maps: ";
const std::string MazeProtocol::BAD         = "910 COMMAND NOT ACCEPTED!";
const std::string MazeProtocol::BAD_GAME    = "920 NO SUCH GAME IS RUNNING!";
const std::string MazeProtocol::BAD_GAME_02 = "930 GAME WITH THIS NAME ALREADY RUNNING!";
const std::string MazeProtocol::BAD_FULL    = "940 GAME IS ALREADY FULL!";
const std::string MazeProtocol::BAD_PROT    = "950 SERVER IS UNABLE TO PROCESS COMMANDS!";
const std::string MazeProtocol::RUN_01      = "110 Game is up and running! To join a game send: JOIN ";
const std::string MazeProtocol::MAP         = "120 MAP ";
const std::string MazeProtocol::BOX         = "130 ";
const std::string MazeProtocol::RUN_04      = "150 SCORE ";
const std::string MazeProtocol::TERM        = "\r\n";

//! Konstruktor tridy MazeProtokol
MazeProtocol::MazeProtocol(Server* server)
{
    _state = STATE_INIT;
    _server = server;
    _queue = _server->GetQueue();
    _gameQueue = new ThreadQueue();
    _message = NULL;
}

//! Destruktor tridy MazeProtocol.
MazeProtocol::~MazeProtocol()
{

}

//! Metoda relizuje zasilani zprav protokolu a reakce na ne
std::string MazeProtocol::GetCommand(std::string message, int id)
{
    //! Na zaklade vnitriniho stavu protpkolu jsou zpravy zpracovany jinak
    switch (_state)
    {
    //! Pro pocatecni stav protokolu
    case(MazeProtocol::STATE_RUNNING):
        //! Zprava o pripojeni noveho klienta vraci uvitaci zpravu a vyzvu k vytvoreni hry
        if (message.length() > 3 && message.compare(0, 3, "NEW") == 0)
        {
            std::string games = "";     //!< Pro ulozeni dostupnych her.
            std::string maps = "";      //!< Pro uloznei dostupnych map.
            std::vector<std::string>* mapVec = Game::GetMaps();

            //! Projdeme vsechny vytvorene hry a pripojime je do retezce pro odeslani.
            for (auto it = _games.begin(); it != _games.end(); ++it)
            {
                if (it->second != NULL)
                    games += it->first + " ";
            }

            for (auto it = mapVec->begin(); it != mapVec->end(); ++it)
                maps += (*it) + " ";

            //! Jako odpoved vratime seznam spustenych her a map ktere jsou dostupne.
            return INIT_01 + games + INIT_02 + maps + TERM;
        }
        //! Po prikazu vytvoreni hry se zkontroluje spravnst argumentu a spusti se hra
        else if (message.length() > 6 && message.compare(0, 6, "CREATE") == 0)
        {
            //! Zprava je prevedena na stream pro snadnejsi rozbor
            std::stringstream s(message.substr(6));
            int limit = 4;                          //!< max pocet hracu hry.
            float speed;                            //!< Rychlost hry.
            std::string name, mapName;              //!< Jmeno hry a mapy pro vytvoreni.

            s >> name >> speed >> mapName; //!< Jednotlive casti zpravy jsou nacteny.

            if (limit > 0 && speed >= 0.5 && speed <= 5 && mapName.empty() == false)
            {
                if (_games[name] != NULL)
                    return BAD_GAME_02 + TERM;

                std::cout << "Creating a game: " << name << ", with map: " <<  mapName << ", "
                            << "with limit " << limit << " and speed: " << speed << std::endl;

                Game *game = new Game(_queue, _gameID++, speed);
                std::string pathMap = std::string(mapName);
                game->LoadMap(pathMap.c_str());
                game->Start(); //->Start(_speed); TODO: v novem vlakne
                _games[name] = game;
                return "";//RUN_01 + TERM;
            }

            return BAD + TERM;
        }
        else if (message.length() > 4 && message.compare(0, 4, "JOIN") == 0)
        {
            std::stringstream rs(message.substr(5));
            std::string name;
            rs >> name;
            Game *game = _games[name];

            //! Kontrola jmeno zadane hry.
            if (game == NULL)
                return BAD_GAME + TERM;

            std::stringstream ss;
            Map * maze = game->GetMap();

            ss << MAP << maze->GetSizeX() << " " << maze->GetSizeY() << TERM;
            _message = new ThreadMessage(0);
            _message->SetData(ss.str().c_str());
            _queue->Send(id, _message);

            //! Hrac je pridan do hry
            if (game->IsInGame(id) == false)
                game->AddPlayer(id);

            return "";
        }
        else if (message.length() >= 4 && message.compare(0, 4, "QUIT") == 0)
        {
            //! V pripade prikazu QUIT je dany hrac odpojen ze hry
            //_game->removePlayer(id);
            return "";
        }
        else if (message.length() > 3 && message.compare(0, 3, "CMD") == 0)
        {
            Player *player = Player::GetPlayer(id);
            if (player == NULL || message.length() <= 4)
                return BAD + TERM;

            if (player->GetState() == PlayerState::KILLED)
                return "150 You are dead!\r\n";

            std::stringstream ss(message.substr(4));
            std::string command;
            ss >> command;

            if (command == "LEFT")
                player->SetDirection(Direction::LEFT);
            else if (command == "RIGHT")
                player->SetDirection(Direction::RIGHT);
            else if (command == "UP")
                player->SetDirection(Direction::TOP);
            else if (command == "DOWN")
                player->SetDirection(Direction::BOTTOM);
            else if (command == "GO")
                player->SetState(PlayerState::GO);
            else if (command == "STOP")
                player->SetState(PlayerState::STOP);
            else if (command == "TAKE")
            {
                if (player->Take())
                    return "150 New key acquired!\r\n";
                else
                    return "150 Unable to take!\r\n";
            }
            else if (command == "OPEN")
            {
                if (player->Open())
                    return "150 Gate is now open!!\rn";
                else
                    return "150 Unable to open!";
            }
            else if (command == "KILL")
            {
                if (player->Kill())
                    return "150 Player killed!!\rn";
                else
                    return "150 Noone around!";
            }
            else
                return "150 Wrong command!";

            //! V pripade prikazu CMD je provedena patricna akce hrace
            return "";
        }
        else
            return BAD + TERM;              //!< Nesrozumitelny prikaz
    default:
        return BAD_PROT + TERM;             //!< Vratime zpravu o nedostupnosti prtokolu.
    }

    return "";                              //!< Zprava nema zadnou odpoved.
}

//! Metoda zajistuje realizaci protokolu na danem serveru,
//! zpracovani zprav ktere mu chodi a odesilani patricnych odpovedi.
void MazeProtocol::HandleServer()
{
    std::chrono::milliseconds duration(10);     //!< Cas pro upsnani mezi kroky.
    ThreadMessage *message = NULL;              //!< Pro cteni a zasilani zprav.
    std::string report;                         //!< Retezec pro ukladani odpovedi.
    std::string command;                        //!< Retezec pro ukladani prijaty prikazu.
    int id;                                     //!< Pro ukladani identifikatoru klienta.

    //! Nacteme ze serveru potrebne ukazatele na klienty pro zajisteni atomicity prace je zkopirujeme.
    std::vector<ClientHandler*> _clients;

    //! Zacneme naslouchat na socketu serveru.
    _server->Listen();

    if (_server->GetState() == Server::STATE_UTL)
    {
        std::cerr << "Server was unable to bind listening socket\n";
        return;
    }

    //! Vytvorime vlakno pro obsluhu klientu, kazdy klient bude pote obsaluhovan zvlast.
    std::thread listener(&Server::Accept, _server);

    _state = STATE_RUNNING;                     //!< Protokol nyni bezi a je plne funkcni.
    while (_server->GetState() != Server::STATE_STOP)
    {
        _clients = _server->GetClients();
        //! Pro kazdeho pripojeneho klienta zkontrolujeme zpravy a zasleme potrebne informace
        for (auto ch = (_clients).begin(); ch != (_clients).end(); ++ch)
        {
            id = (*ch)->GetID();
            message = _queue->Get(DEFAULT_THREADS_CONTRLER_ID, id);//NetThread::Get(0, &_queue);//, _mutex);

            //! Zkontroluji se prijate zpravy od daneho klienta.
            if (message != NULL)
            {
                command = message->GetData(0);
                std::cout << "S<<<---C" << id << ": " << command << std::endl;
                report = GetCommand(command, id);

                if (report != "")
                {
                    std::cout << "S--->>>C" << id << ": " << report << std::endl;
                    //! Vytvori se nova zprava pro vlakno zpracovavajici klienta.
                    message = new ThreadMessage(0);
                    //! Zprava se naplni zpravou konkretniho stavu protokolu
                    message->SetData(report.c_str());
                    //! A je zarazena do fronty
                    _queue->Send(id, message);
                }
            }
        }
        std::this_thread::sleep_for(duration);
    }

    for (auto it = _games.begin(); it != _games.end(); ++it)
        if (it->second != NULL)
            (it->second)->Stop();


    _server->Stop();                //!< Zastavime server
    listener.join();                //!< A pockame na uknceni behu nasluchace.
    for (auto it = _games.begin(); it != _games.end(); ++it)
        if (it->second != NULL)
            delete it->second;
}

//! Nastaveni stavu protokolu.
void MazeProtocol::SetState(int state)
{
    _state = state;
}

//! Ziskani stavu protkolu.
int MazeProtocol::GetState()
{
    return _state;
}
