//! Soubor maze_protocol.h
/*! Hlavickovy soubor obsahujici definici tridy Maze protokol
    Vse je napsano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __MazeProtocol_H__
#define __MazeProtocol_H__

#include <iostream>
#include <string>
#include <thread>
#include <sstream>
#include <atomic>
#include "server.h"
#include "../game/game.h"

//! Trida MazeProtocol
/*! Trida MazeProtocol zajistuje vnitrni komunikaci serveru s klientem. Je postavena
    na zasilani zprav a zmen na zakalde vnitrniho stavu protkolu.
 */
class MazeProtocol
{
    private:
        //! Privatni atributy.
        int _state;                             //!< Vnitrni stav protokolu.
        ThreadQueue* _queue;                    //!< Front pro zasilani zprav mezi vlakny.
        ThreadQueue* _gameQueue;                 //!< Pro cteni zprav z her.
        ThreadMessage *_message;                //!< Pro cteni a zasilani zprav.
        Server* _server;                        //!< Server nad kterym protokol pracuje.
        int _gameID = 0;                        //!< Pro sekvenci id her.
        std::map<std::string, Game*> _games;    //!< Mapa vytvorenych her.

        //! ZPRAVY PROTOKOLU
        static const std::string INIT;
        static const std::string INIT_01;
        static const std::string INIT_02;
        static const std::string INIT_03;
        static const std::string INIT_04;
        static const std::string BAD_01;
        static const std::string BAD;
        static const std::string BAD_GAME;
        static const std::string BAD_GAME_02;
        static const std::string BAD_FULL;
        static const std::string BAD_PROT;
        static const std::string MAP;
        static const std::string RUN_01;
        static const std::string RUN_02;
        static const std::string RUN_03;
        static const std::string RUN_04;
        static const std::string BOX;
        static const std::string TERM;

    public:
        //! Verejne atributy a konstanty
        static const int P_ERROR        = -1;   //!< Hodnoty Navratovych kodu: chyba.
        static const int P_OK           =  0;   //!< Metoda probehla vporadku.

        static const int STATE_INIT     = 100;  //!< Stav po inicializaci.
        static const int STATE_RUNNING  = 200;  //!< Behovy stav protkolu.
        static const int STATE_FINISHED = 300;  //!< Koncovy stav protkolu.
        static const int STATE_BAD      = 900;  //!< Protokol dany prikaz nepripousti

        //! Verejne metody.
        MazeProtocol(Server* server);           //!< Konstruktor tridy protkol.
        virtual ~MazeProtocol();                //!< Destruktor tridy protkol.

        //! Metoda ridici chod protokolu. Prijmani a generovani zprav na zaklade vnitrniho stavu.
        std::string GetCommand(std::string command, int id);

        void HandleServer();                    //!< Metoda Realiuzjici beh serveru a zpracovani klientu
        //void SendMapData(Game* game);           //!< Metoda pro jednorazove zasilani dat

        void SetState(int state);               //!< Metoda nastavuje vnitrni sav protkolu
        int GetState();                         //!< Metoda pro ziskani vnitrniho stavu protokolu
};

#endif // __MazeProtocol_H__
