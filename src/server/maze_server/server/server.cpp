//! Soubor server.cpp
/*! Hlavickovy soubor obsahujici definici tridy Server.
    Ta zajistuje inicializaci socketu, spravne naslouchani na specifickem
    portu. A zpracovani jednotlivych klientu kteri se na server pripoji.
    Vse je psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#include "server.h"

//! Konstruktor tridy Server, zajisti vhodnou inicializaci
/*! Ulozi vsechny potrebne informace do atributu tridy.

    \sa ~Server(), Listen() Accept() and Stop()
 */
Server::Server(std::string port, int limit/*, AF_UNSPEC, SOCK_STREAM*/)
{
    _currentID = 0;
    _limit = limit;
    _port = port;
    _state.store(SocketInitialization());
}

//! Destruktor
/*! zajisti pripadne uzavreni neuzvarenych socketu a ukonceni vlaken pracujicich
    s klienty. Na systemech windows je navic potreba zavolat funkci WSACleanip();

    \sa Server(), Listen(), Accept() and Stop()
 */
Server::~Server()
{
    if (_socket)
        closesocket(_socket);

    #ifdef _WIN32
    WSACleanup();
    #endif // _WIN32
}

//! Inicializace socketu, je nutna pro spravnou funkcnost na systemech windows
/*! \sa Server(), ~Server(), Listen() and Accept()
 */
int Server::SocketInitialization()
{
    #ifdef _WIN32
    WSADATA wsaData;

    if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0) {
        return Server::STATE_UTL;
    }
    #endif // _WIN32

    return Server::STATE_OK;
}

//! Metoda realizue otevreni socketu a nastaveni pro naslouchani
/*! Metoda pracuje se sokcety sandardni zpusobem, realizuje spojeni
    pomoci TCP protokolu na portu serveru za pomoci IPv4 i 6

    \return informace o uspesnosti metody.
    \sa Server(), ~Server(), Accept() and Stop()
 */
int Server::Listen()
{
    //! Definice struktury a inicializace ukazetlu.
    struct addrinfo *serverinfo = NULL,
                    *p = NULL,
                    hints;

    //! Nastavime strukturu addrinfo potrebnymi daty.
    ZeroMemory( &hints, sizeof(hints) );    //!< Nejdrive vynulujeme pamet.
    hints.ai_family = AF_INET; //UNSPEC;            //!< Nastavime na libovolny IP.
    hints.ai_socktype = SOCK_STREAM;        //!< Pouzijeme socket stream.
    hints.ai_protocol = IPPROTO_TCP;        //!< Specifikujeme TCP protokol.
    hints.ai_flags = AI_PASSIVE;			//!< Spojeni

    //! Ziskame seznam adres pro pripojeni na dany server
    if (getaddrinfo(NULL, _port.c_str(), &hints, &serverinfo) != 0) {
        _state.store(Server::STATE_UTL);
        return Server::STATE_UTL;
    }

    //! V cyklu projdeme senzam po prvcich a pokusime se navazat spojeni.
    int res;                                //!< Pro kontorlu hodnot funkci.
    for (p = serverinfo; p != NULL; p = p->ai_next)
    {
        //! Pokusime se vytvorit socket na danem portu.
        _socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (_socket == INVALID_SOCKET) {
            std::cerr << strerror(errno) << std::endl;
            _state.store(Server::STATE_UTL);
            return Server::STATE_UTL;
        }

        //! na server. V pripade selhane zkusime dalsi polozku.
        res = bind(_socket, p->ai_addr, p->ai_addrlen);
        if (res == -1) {            //!< Zkontrolujeme zda bindovani zdarilo.
            closesocket(_socket);   //!< Pokud ne uzvareme ziskany socket.
            _socket = INVALID_SOCKET;   //!< Nastavime jej na nevalidni.
            continue;               //!< A Zkusime pokracovat dalsi poolozkou.
        }

        _addrinfo = *p;             //!< Ulozime si informace o pripojeni.
        break;                      //!< V pripade navazani spojeni koncime.
    }

    freeaddrinfo(serverinfo);       //!< Volame funkci pro uvolneni seznamu.

    //! Zkontrolujeme zda bylo spojeni navazano
    if (_socket == INVALID_SOCKET) {
        std::cerr << strerror(errno) << std::endl;
        _state.store(Server::STATE_UTL);
        return Server::STATE_UTL;
    }

    // Try to listen at the socket
	if (listen(_socket, _limit) == -1)
    {
        std::cerr << strerror(errno) << std::endl;
        _state.store(Server::STATE_UTL);
        return Server::STATE_UTL;
    }

#ifdef _WIN32
    u_long iMode = 0;
    res = ioctlsocket(_socket, FIONBIO, &iMode);
#else
    res = fcntl(_socket, F_SETFL, O_NONBLOCK);
#endif
    if (res != NO_ERROR)
        return Server::STATE_UTL;

    return Server::STATE_OK;
}

//! Metoda zajistuje naslouchani na portu pro prijimani spojeni klientu
/*! Pro kazdeho klienta ktery se pripoji na dany socket je vytvorena instance
    tridy ClientHandler, ten je inicializovan a spusten. Zpracovani daneho
    klienta probiha v samostatnem vlakne a naslouchani tak muze pokracovat.
    V pripade vypnuti serveru je smycka prerusena a vsechny

    \sa Server(), ~Server(), Listen() and Stop()
 */
void Server::Accept()
{
    // Awating connection
    struct sockaddr client_addr;
    //socklen_t sin_size;
    //sin_size = sizeof client_addr;
    unsigned int clientSocket;

    while (_state.load() != Server::STATE_STOP)
    {
        clientSocket = accept(_socket, NULL, NULL);//(struct sockaddr *)&client_addr, &sin_size);

        //! Nove pripojeny klient je zpracovavan v nove vytvorenem vlakne
        if (clientSocket != INVALID_SOCKET)
        {
            ClientHandler *clienthandler = new ClientHandler(&_queue, ++_currentID, clientSocket);
            //std::cout << "Creating client: " << _currentID << " " << clienthandler->GetID() << std::endl;
            AddClient(clienthandler);
            clienthandler->Start();
        }
    }

    for (auto ch = _clients.begin(); ch != _clients.end(); ++ch)
    {
        (*ch)->Stop();
    }
}

//! Metoda uzavira sockety serveru a nastavi jeho stav  otevreni socketu a nastaveni pro naslouchani
/*! \sa Server(), ~Server(), Listen() and Accept()
 */
void Server::Stop()
{
    if (_socket != INVALID_SOCKET)
    {
        closesocket(_socket);
        _socket = INVALID_SOCKET;
    }

    _state.store(STATE_STOP);
}

void Server::SetState(int state)
{
    _state.store(state);
}


int Server::GetState()
{
    return _state.load();
}

ThreadQueue* Server::GetQueue()
{
    return &_queue;
}

void Server::AddClient(ClientHandler* handler)
{
    _mutex.lock();
    _clients.push_back(handler);
    _mutex.unlock();
}


std::vector<ClientHandler*> Server::GetClients()
{
    _mutex.lock();
    std::vector<ClientHandler*> clientsCopy = _clients;
    _mutex.unlock();
    return clientsCopy;
}
