//! Soubor server.h
/*! Hlavickovy soubor obsahujici definici tridy Server.
    Ta zajistuje inicializaci socketu, spravne naslouchani na specifickem
    portu. A zpracovani jednotlivych klientu kteri se na server pripoji.
    Vse je psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __SERVER_H__
#define __SERVER_H__

#include <iostream>
#include <string>
#include <vector>
#include <thread>
#include <thread>
#include <mutex>
#include <map>
#include <queue>
#include <atomic>
#include "client_handler.h"
#include "../../../common/net_thread.h"
#include "socket_portable.h"
#include <fcntl.h>

//! Trida Server
/*! Trida Server zastinuje praci se sockety. Konstruktor pouze ulozi dulezita
 *  data pro zadane spojeni a volanim metod Listen, Send a GetLine muze uzivatel
 *  snadno komunikovat s klienty kteri na danem portu navazou se serverem
 *  spojeni.
 */
class Server
{
    private:
        //! Privatni atributy.
        struct addrinfo _addrinfo;              //!< Informace o konfiguraci socketu.
        std::string _port;                      //!< Port pro naslouchani.
        std::atomic<int> _state;                //!< Atomicka promenna pro uchovani stavu serveru.
        int _limit;                             //!< Max pocet pripojeni pro server
        unsigned int _socket, _currentID;       //!< Ulozeni socketu a sekvence ID klientu.

        std::mutex _mutex;                      //!< Pro nacitani a ukladni klientu.
        ThreadQueue _queue;                     //!< Fronta pro zasilani zprav klientu.
        std::vector<ClientHandler*> _clients;   //!< Vektor pripojenych klientu

        //! Privatni metody.
        int SocketInitialization();             //!< Inicializace socketu

    public:
        //! Verejne atributy a konstanty
        static const int STATE_OK = 0;          //!< Stavy serveru: Vse je vporadku.
        static const int STATE_UTL = -1;        //!< Neschopen naslouchat na socketu.
        static const int STATE_STOP = -2;       //!< Server je zastaven.

        //! Verejne metody.
        Server(std::string port, int limit);    //!< Konsturktor tridy.
        virtual ~Server();                      //!< Destruktor.

        int Listen();                           //!< Metoda zajistuji naslouchani na socketu.
        void Accept();                          //!< Pro zpracovani klientu kteri se pripji.
        void Stop();                            //!< Zastaveni serveru, uvolneni sockeetu.
        void SetState(int state);               //!< Metoda pro nastaveni stavu serveru.
        int GetState();                         //!< Metoda pro ziskani stavu serveru.
        ThreadQueue* GetQueue();                //!< Metoda pro ziskani fronty zprav pro komunikaci.
        void AddClient(ClientHandler*);         //!< Metoda pro diani kleinta do vektoru.
        std::vector<ClientHandler*> GetClients();   //!< Metoda pro ziskani klientu.
};

#endif // __SERVER_H__
