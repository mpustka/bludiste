//! Soubor socket_portable.h
/*! Hlavickovy soubor zajistuje vlozeni spravnych hlavickovych souboru
    na zaklade platformy. Sitova cast hry pouziva pro sitovani sockety.
    Vse je psano rucne.

    Autori:     Martin Roncka   (xronck00@stud.fit.vutbr.cz),
                Michal Pustka   (xpustk00@stud.fit.vutbr.cz).

    Projekt:    ICP_bludiste
 */

#ifndef __SOCKET_PORTABLE_H__
#define __SOCKET_PORTABLE_H__

#include <cstring>
#include <unistd.h>

//! Vkladani hlavickovych souboru dle platformny
#ifdef _WIN32

    #define _WIN32_WINNT 0x501
    #include <windows.h>
    #include <winsock2.h>
    #include <ws2tcpip.h>

#else

    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
    #include <fcntl.h>

    #define closesocket(socket) close(socket)
    #define ZeroMemory(memory, size) std::memset(memory, 0, size)
    #define INVALID_SOCKET ((unsigned int) -1)
    #define SOCKET_ERROR (-1)
    #define NO_ERROR (0)

#endif

#endif // __SOCKET_PORTABLE_H__
